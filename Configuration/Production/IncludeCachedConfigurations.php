<?php
if (FLOW3_PATH_ROOT !== '/serverdata/worldboxglobal.ch/dev.comgate/html/comsys/' || !file_exists('/serverdata/worldboxglobal.ch/dev.comgate/html/comsys/Data/Temporary/Production/Configuration/ProductionConfigurations.php')) {
	unlink(__FILE__);
	return array();
}
return require '/serverdata/worldboxglobal.ch/dev.comgate/html/comsys/Data/Temporary/Production/Configuration/ProductionConfigurations.php';
?>