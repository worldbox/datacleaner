

comSys = {
		
		
	messages : {
	
	
		showMessage : function (message, type) {
			var message = '<div class="message">' + message + '</div>'
			jQuery("#messages").html(message);
			jQuery("#messages").bind("click", function () {
				//comSys.messages.resetMessage();
			});
			setTimeout(function () {
					comSys.messages.resetMessage();
				}, 20000);
		},
		
		resetMessage : function () {
			jQuery("#messages").html("");
		},

	},
	
		
	navigation : {
		
		/**
		 * Initialization of the main navigation.
		 */
		init : function () {
			
			// Mouse over events
			jQuery("#navigation .navigationItem").bind ("mouseover", function () {
				jQuery(this).addClass("over");
			});
			jQuery("#navigation .navigationItem").bind ("mouseout", function () {
				jQuery(this).removeClass("over");
			});
			
		}
		
	},	
		
		
	winMatchComp : {
		
		
		searchType : "PARENT_COMPANY",
		
		init : function () {
	
			comSys.winMatchComp.initProgMenu();
		},
		
		/**
		 * Defines what happens on a click on a program.
		 */
		initProgMenu : function () {
			
			jQuery("#dbComgateNavigation .parentCompanyMatching").click (function () {
				var progLink = jQuery(this).find("a").attr("href");
				comSys.winMatchComp.loadProgram(progLink, false);
				
				return false;
			});
			
			jQuery("#dbComgateNavigation .noWinMatching").click (function () {
				var progLink = jQuery(this).find("a").attr("href");
				comSys.winMatchComp.loadProgram(progLink, true);
				return false;
			});
		},
		
		/**
		 * Given a link the method loads the program and displays its html representation.
		 */
		loadProgram : function (progLink, noWin) {
			
			if (progLink != undefined) {
				jQuery("#globalLoader").show();
				jQuery.get(progLink, function (response) {
					jQuery("#dbComgateListWrapper").html(response);	
					jQuery("#globalLoader").hide();
					
					
					if (noWin == true) {
						// Init program for no win matching
						comSys.winMatchComp.initLoadedDbComgateNoWin();
					} else {
						// Init program for parent company matching
						comSys.winMatchComp.initLoadedProgram();
					}
				});
			}
		},
		
		
		openSlider : function () {
			var slider = jQuery("#toggleDbProg");
			jQuery(slider).removeClass("right");
			jQuery(slider).addClass("down");
			jQuery("#slider").slideDown(100, function () {
			});
		},
		
		
		closeSlider : function () {
			var slider = jQuery("#toggleDbProg");
			jQuery(slider).removeClass("down");
			jQuery(slider).addClass("right");
			jQuery("#slider").slideUp (100, function () {
			});
		},
		
		
		initLoadedProgram : function () {
			
			jQuery("#toggleDbProg").bind("click", function () {
				if (jQuery(this).hasClass("down")) {
					comSys.winMatchComp.closeSlider();	
				} else {
					comSys.winMatchComp.openSlider();	
				}
			});
			
			// Init the switcher (Radio buttons) between the two search types
			jQuery("#countrySearchForm").hide();
			jQuery("#searchType").find("input").change (function () {
				
				var value = jQuery(this).attr("value")
				comSys.winMatchComp.searchType = value;
				jQuery("#dbComgateCompList").html("");
				if (value == "PARENT_COMPANY") {
					jQuery("#parentCompanies").show();
					jQuery("#countrySearchForm").hide();
					jQuery("#parentCompanySearchForm").show();
				} else {
					jQuery("#parentCompanies").hide();
					jQuery("#countrySearchForm").show();
					jQuery("#parentCompanySearchForm").hide();
				}
				
			});
			
			// UInit search form (By parent company)
			jQuery("#winMatchSearchByParentCompanyButton").bind ("click", function () {
				comSys.winMatchComp.searchDbComgateByParentCompany();
				return false;
			});
			
			// UInit search form (By country)
			jQuery("#winMatchSearchByCountryButton").bind ("click", function () {
				comSys.winMatchComp.searchDbComgateByCountry();
				return false;
			});
					
			comSys.winMatchComp.initDbComgateList();
		},
		
		
		
		/**
		 * Inits the list of DbComgate objects representing parent companies.
		 */
		initDbComgateList : function () {
			// Init edit buttons
			jQuery(".showDbCompLink").bind("click", function () {
				
				jQuery("#dbComgateCompList").html("");
				jQuery("#dbComgateCompEdit").html("");
				jQuery("#toggleDbProg").show();
				comSys.winMatchComp.getDbComgateComp(this)
				jQuery("#shownDbComgates").find(".listItem").removeClass("selected");
				jQuery(this).parentsUntil(".listItem").parent().addClass("selected");
				return false;
			});
		},
		
		
		/**
		 * Does a search with the values in the search form. Shows a list
		 * of dbComgate objects that have open cases to edit.
		 */
		searchDbComgateByParentCompany : function () {
			
			var formData		= jQuery("#parentCompanySearchForm").serialize();
 			var url				= jQuery("#parentCompanySearchForm").attr("action");
 			jQuery("#dbComgateCompList").html("");
 			jQuery("#globalLoader").show();
			jQuery.ajax({
				type	: 'POST',
				url		: url,
				data	: formData,
				success	: function (data) {
					jQuery("#dbComgateListContainer").html(data);
					comSys.winMatchComp.initDbComgateList();
					jQuery("#globalLoader").hide();
				}
			});
			 
		},
		
		
		/**
		 * Shows open dbComgateComp objects to be edited that belong to
		 * the selected country.
		 */
		searchDbComgateByCountry: function () {
			
	
			var formData		= jQuery("#countrySearchForm").serialize();
 			var url				= jQuery("#countrySearchForm").attr("action");
			
 			jQuery("#globalLoader").show();
			jQuery.ajax({
				type	: 'POST',
				url		: url,
				data	: formData,
				success	: function (data) {
					jQuery("#dbComgateCompList").html(data);
					comSys.winMatchComp.closeSlider();
					comSys.winMatchComp.initDbComgateComp();
					jQuery("#globalLoader").hide();
				}
			});
			
		},
		
		
		
		
		getDbComgateCompJqXHR : null,
		
		/**
		 * 
		 */
		getDbComgateComp : function (link) {
			
			if (comSys.winMatchComp.getDbComgateCompJqXHR != null) {
				comSys.winMatchComp.getDbComgateCompJqXHR.abort()
			}
			jQuery("#globalLoader").show();
			comSys.winMatchComp.getDbComgateCompJqXHR = jQuery.get(link, function (response) {
				jQuery("#globalLoader").hide();
				jQuery("#dbComgateCompList").html(response);	
				comSys.winMatchComp.closeSlider();
				comSys.winMatchComp.initDbComgateComp();
			});
		},

		
		/**
		 * Holds ajax jqXHR object. Can be used to abort in case of multiple reuqests.
		 */
		switchDbCompJqXHR : null,
		
				
		
		/**
		 * The currently loaded dbComgateComp elements (in the list) are
		 * initialized. 
		 */
		initDbComgateComp : function () {
			
			jQuery("#dbComgateCompList").fadeTo(0, 1.0);
			// Init load previous
			jQuery("#loadPreviousDbComgateComp").bind("click", function () {
				jQuery("#dbComgateCompList").fadeTo(300, 0.4);
				jQuery("#globalLoader").show();
				var actionLink = jQuery(this).attr("href");
				jQuery.get(actionLink, function (response) {
					jQuery("#globalLoader").hide();
					if (response == "done") {
						jQuery("#shownDbComgates").find(".listItem.selected").hide();
						jQuery("#dbComgateCompList").html("No more cases available");
						return false;
					} 
					
					jQuery("#dbComgateCompList").html(response);
					jQuery("#dbComgateCompList").fadeTo(0, 1.0);
					comSys.winMatchComp.initDbComgateComp();
				});
				return false;
			});
			
			
			jQuery("#searchName").bind( "keypress", function (event) {
				
				 if ( event.which == 13 ) {
					 event.preventDefault();
					 var actionLink = jQuery("#companySearchLink").attr("href");
					 comSys.winMatchComp.performCompanySearch(actionLink);	
					 
				 }
			});
			
			// Init load nex function
			jQuery("#loadNextDbComgateComp").bind("click", function () {
				
				jQuery("#dbComgateCompList").fadeTo(300, 0.4);
				var actionLink = jQuery(this).attr("href");
				jQuery("#globalLoader").show();
				jQuery.get(actionLink, function (response) {
					jQuery("#globalLoader").hide();
					if (response == "done") {
						jQuery("#shownDbComgates").find(".listItem.selected").hide();
						jQuery("#dbComgateCompList").html("No more cases available");
						return false;
					} 
					jQuery("#dbComgateCompList").html(response);
					jQuery("#dbComgateCompList").fadeTo(0, 1.0);
					comSys.winMatchComp.initDbComgateComp();
				});
				return false;
			});
			
			// Init match search
			jQuery("#companySearchLink").bind("click", function () {
				var actionLink = jQuery(this).attr("href");
				comSys.winMatchComp.performCompanySearch(actionLink);
				return false;
			});
			
			// Init button to create new company
			jQuery("#createNewCompany").bind("click", function () {
				
				var conf = confirm("Do you want to open a new Company?");
			    if(conf == true){
			    	jQuery("#globalLoader").show();
			    	var actionLink = jQuery(this).attr("href");
			    	// Set win of db comgate
			    	jQuery.get(actionLink, function (response) {
			    		
			    		jQuery("#globalLoader").hide();
			    		if (response != 'fail') {
							jQuery("#loadNextDbComgateComp").click();
							comSys.messages.showMessage("Successfully created new company with WIN " + response);
						} else {
							comSys.messages.showMessage("Failed to create new company.");
						}
					});
			    }
				return false;
			});
			
			
			// Button to inactivate a company relation
			jQuery("#inactivateRelation").bind("click", function () {
				var conf = confirm("Do you want to inactivate the relation?");
			    if(conf == true){
			    	jQuery("#globalLoader").show();
			    	var actionLink = jQuery(this).attr("href");
			    	// Set win of db comgate
			    	jQuery.get(actionLink, function (response) {
			    		
			    		jQuery("#globalLoader").hide();
			    		
			    		if (response != 'fail') {
							jQuery("#loadNextDbComgateComp").click();
							comSys.messages.showMessage("Successfully inactivated the relation");
						} else {
							comSys.messages.showMessage("Inactivation of the relation failed.");
						}
						
					});
			    }
				return false;
			});	
			
			comSys.winMatchComp.initMatchList();
		},
		
		
		/**
		 * Inits the list of potential matches. Makes it possible ot select one as the match.
		 */
		initMatchList : function () {
			
			jQuery("#potentialMatches .matchCompanyLink").bind("click", function () {
				
				var listItem 	= jQuery(this).parentsUntil(".listItem").parent(); 
				jQuery(listItem).addClass("selected");
				var conf 		= confirm("Do you want to match with that company?");
						
			    if(conf == true){
			    	jQuery("#globalLoader").show();
			    	var actionLink = jQuery(this).attr("href");
			    	// Set win of db comgate
			    	jQuery.get(actionLink, function (response) {
			    		
			    		if (response == 'ok') {
							jQuery(listItem).removeClass("selected");
							jQuery("#loadNextDbComgateComp").click();
							comSys.messages.showMessage("Company successfully matched.");
						} else {
							comSys.messages.showMessage("Failed to match company.");
						}
			    		jQuery("#globalLoader").hide();
					});
			    	
			    } else {
			    	jQuery(listItem).removeClass("selected");
			    }
			    
				return false;
			});
		},
				
		
		
		/**
		 * Holds ajax jqXHR object. Can be used to abort in case of multiple reuqests.
		 */
		searchDbCompJqXHR : null,
		
		/**
		 * Executed at click on search button in edit mask. Performs
		 * a company search and shows result list.
		 */
		performCompanySearch : function (actionLink) {
		
			if (comSys.winMatchComp.searchDbCompJqXHR != null) { 
				comSys.winMatchComp.searchDbCompJqXHR.abort()
			}
			var dbComgateComp  		= jQuery("#currentDbComgateComp").attr("value");
			var searchName	   		= jQuery("#searchName").attr("value");
			var selectedCountry 	= jQuery("#countrySearch").val();
			var data = {};
			data.dbComgateCompId 	= dbComgateComp;
			data.searchName 		= searchName;
			data.countryCode 		= selectedCountry;
		
			jQuery("#potentialMatches").css({ opacity: 0.1 });
			jQuery("#globalLoader").show();
			comSys.winMatchComp.searchDbCompJqXHR = jQuery.get(actionLink, data, function (response) {
				jQuery("#matchList").html(response);	
				comSys.winMatchComp.searchDbCompJqXHR = null;
				comSys.winMatchComp.initMatchList();
				jQuery("#globalLoader").hide();
			});
		},
		
		/*******************************************************************************************
		 * NO WIN MATCHING FUNCTIONS
		 * The functions needed for the matching of companies that do not have a WIN and need to
		 * be aligned with cimsys
		 ******************************************************************************************/
		
		
		initLoadedDbComgateNoWin : function () {
			
			
			jQuery("#loadPreviousDbComgateComp").bind("click", function () {
				var url = jQuery(this).attr("href");
				comSys.winMatchComp.loadDbComgateNoWin(url)
				return false;
			});
			jQuery("#loadNextDbComgateComp").bind("click", function () {
				var url = jQuery(this).attr("href");
				comSys.winMatchComp.loadDbComgateNoWin(url)
				return false;
			});
			
			jQuery("#companySearchLink").bind("click", function () {
				var url = jQuery(this).attr("href");
				comSys.winMatchComp.cimsysSearchNoWinAction(url)
				return false;
			});
			
			jQuery("#searchName").bind( "keypress", function (event) {
				
				 if ( event.which == 13 ) {
					 event.preventDefault();
					 var actionLink = jQuery("#companySearchLink").attr("href");
					 comSys.winMatchComp.cimsysSearchNoWinAction(actionLink)	 
				 }
			});
			
			
			jQuery("#createNewCompany").bind("click", function () {
				
				var conf = confirm("Do you want to open a new Company?");
			    if(conf == true){
			    	jQuery("#globalLoader").show();
			    	var actionLink = jQuery(this).attr("href");
			    	// Set win of db comgate
			    	
			    	
			    	jQuery("#loadedDbComgate").css({ opacity: 0.6 });
			    	jQuery.get(actionLink, function (response) {
			    		
			    		jQuery("#globalLoader").hide();
			    		if (response != 'fail') {
							jQuery("#loadNextDbComgateComp").click();
							comSys.messages.showMessage("Successfully created new company with WIN " + response);
						} else {
							jQuery("#loadedDbComgate").css({ opacity: 1.0 });
							comSys.messages.showMessage("Failed to create new company.");
						}
			    		
					});
			    }
				return false;
			});
			
			
			jQuery("#inactivateRelation").bind("click", function () {
				
				var conf = confirm("Do you want to inactivate the Company?");
			    if(conf == true){
					jQuery("#globalLoader").show();
					
					var url = jQuery(this).attr("href");
					jQuery("#loadedDbComgate").css({ opacity: 0.6 });
			    	jQuery.get(url, function (response) {
			    		
			    		jQuery("#globalLoader").hide();
			    		if (response != 'fail') {
							jQuery("#loadNextDbComgateComp").click();
							comSys.messages.showMessage("Inactivated company. Loading next...");
						} else {
							jQuery("#loadedDbComgate").css({ opacity: 1.0 });
							comSys.messages.showMessage("Failed to ianctivate company.");
						}
			    		
					});
			    }
				return false;
			});
			
			comSys.winMatchComp.initMatchListNoWin();
		},
		
		
		
		initMatchListNoWin : function () {
			
			jQuery("#potentialMatches .matchCompanyLink").bind("click", function () {
				
				var listItem 	= jQuery(this).parentsUntil(".listItem").parent(); 
				jQuery(listItem).addClass("selected");
				var conf 		= confirm("Do you want to match with that company?");
					
			    if(conf == true){
			    	jQuery("#loadedDbComgate").css({ opacity: 0.6 });	
			    	jQuery("#globalLoader").show();
			    	var actionLink = jQuery(this).attr("href");
			    	// Set win of db comgate
			    	jQuery.get(actionLink, function (response) {
			    		
			    		if (response == 'ok') {
							jQuery(listItem).removeClass("selected");
							jQuery("#loadNextDbComgateComp").click();
							comSys.messages.showMessage("Company successfully matched.");
						} else {
							jQuery("#loadedDbComgate").css({ opacity: 1.0 });
							comSys.messages.showMessage("Failed to match company.");
						}
			    		jQuery("#globalLoader").hide();
					});
			    	
			    } else {
			    	jQuery(listItem).removeClass("selected");
			    }
			    
				return false;
			});
		},
		
		
		cimsysSearchNoWinAction : function (url) {
			
			
			if (comSys.winMatchComp.searchDbCompJqXHR != null) { 
				comSys.winMatchComp.searchDbCompJqXHR.abort()
			}
	
			var searchName	   		= jQuery("#searchName").attr("value");
			var selectedCountry 	= jQuery("#countrySearch").val();
			var data = {};
			data.searchName 		= searchName;
			data.countryCode 		= selectedCountry;
		
			jQuery("#potentialMatches").css({ opacity: 0.1 });
			jQuery("#globalLoader").show();
			comSys.winMatchComp.searchDbCompJqXHR = jQuery.get(url, data, function (response) {
				jQuery("#matchList").html(response);	
				comSys.winMatchComp.searchDbCompJqXHR = null;
				comSys.winMatchComp.initMatchList();
				comSys.winMatchComp.initMatchListNoWin();
				jQuery("#globalLoader").hide();
			});
			
		},
		
		loadDbComgateNoWin : function (url) {
			jQuery("#globalLoader").show();
			jQuery("#loadedDbComgate").css({ opacity: 0.6 });
			jQuery.get(url,{}, function(response) { 
				jQuery("#dbComgateListWrapper").html("");
				jQuery("#dbComgateListWrapper").hide();
				jQuery("#dbComgateListWrapper").html(response);
				jQuery("#dbComgateListWrapper").show();
				jQuery("#globalLoader").hide();
				comSys.winMatchComp.initLoadedDbComgateNoWin();
				jQuery("#loadedDbComgate").css({ opacity: 1.0 });
			});
		}
		
		
	},
	
	
	
	
	dbComgate : {
		
		/**
		 * Lock for the save process.
		 */
		saveInProgress : false,
		
		/**
		 * Indicates whether the dbcomgates are listed by a search or by prev/next
		 */
		dbComgateSearchDisplayed : false,
		
		/**
		 * Initialization of the dbComgate list. 
		 */
		init : function () {
			
			jQuery("#dbComgateNavigation .listItem").bind("click", comSys.dbComgate.loadProgram);
		},
		
		
		/**
		 * Shows the next N dbComgate objects in the list.
		 */
		loadProgram : function () {
			
			jQuery("#dbComgateNavigation .listItem").removeClass("selected");
			jQuery(this).addClass("selected");
			
			jQuery("#globalLoader").show();
			jQuery("#dbComgateListWrapper").fadeOut(200);
			var url = jQuery(this).find("a").attr("href");
			jQuery.get(url,{},function(response){ 
				
				jQuery("#dbComgateListWrapper").html("");
				jQuery("#dbComgateListWrapper").html(response);
				jQuery("#globalLoader").hide();
				jQuery("#dbComgateListWrapper").fadeIn(200);
				
				jQuery("#loadNextDbComgate").bind("click", comSys.dbComgate.loadNextDbComgate);
				jQuery("#loadPreviousDbComgate").bind("click", comSys.dbComgate.loadPreviousDbComgate);
				jQuery("#dbComgateList .dbComgateEditLink").bind("click", comSys.dbComgate.loadDbComgateObject);
				jQuery("#dbComgateListSearchFormButton").bind("click", comSys.dbComgate.searchDbComgate);
				jQuery("#dbComgateList .dbComgateEditLink").click();
				
				
			 });
			
			return false;
		},
		
		
		loadNextDbComgate : function () {
			comSys.dbComgate.dbComgateSearchDisplayed = false;	
			var url = jQuery("#loadNextDbComgateLink").attr("href")
			comSys.dbComgate.replaceCurrentDbComgate(url);
			
		},
		
		loadPreviousDbComgate : function () {
			comSys.dbComgate.dbComgateSearchDisplayed = false;	
			var url = jQuery("#loadPreviousDbComgateLink").attr("href")
			comSys.dbComgate.replaceCurrentDbComgate(url);
			
		},
		
		loadFirstDbComgate : function () {
			var url = jQuery("#dbComgateLoadFirstLink").attr("href");
			comSys.dbComgate.replaceCurrentDbComgate(url);
		},
		
		abortSearchResults : function () {
			comSys.dbComgate.dbComgateSearchDisplayed = false;	 
		},
		
		replaceCurrentDbComgate : function (url) {
			
			jQuery("#globalLoader").show();
			
			jQuery("#dbComgateEdit").fadeIn(200);
			jQuery("#dbComgateEdit").html("");
			jQuery.get(url,{},function(response) { 
				
				jQuery("#shownDbComgates").html("");
				jQuery("#shownDbComgates").hide();
				jQuery("#shownDbComgates").html(response);
				jQuery("#globalLoader").hide();
				jQuery("#dbComgateList .dbComgateEditLink").bind("click", comSys.dbComgate.loadDbComgateObject);
				jQuery("#dbComgateList .dbComgateEditLink").click();
				jQuery("#shownDbComgates").fadeIn(200);
			});
		},
		
		
		cancleSerach : function () {
			
			jQuery("#exitSearchResults").fadeOut(function () {
				jQuery("#prevNext").fadeIn();
			});
			
			comSys.dbComgate.loadFirstDbComgate();
					
		},
		
		searchDbComgate : function () {
				 
			var url 			= jQuery("#dbComgateListSearchFormLink").attr("href");
			var searchString 	= jQuery("#dbComgateListSearchFormSearchString").attr("value");
			
			if (searchString.length < 3) {
				return false;
			}
			jQuery("#prevNext").fadeOut(function () {
				jQuery("#exitSearchResults").fadeIn();
			});
			
			jQuery("#exitSearchResults").unbind();
			jQuery("#exitSearchResults").bind("click", comSys.dbComgate.cancleSerach);
			comSys.dbComgate.dbComgateSearchDisplayed = true;

			jQuery.get(url,{searchString : searchString},function(response){ 
				
				jQuery("#shownDbComgates").html("");
				jQuery("#shownDbComgates").html(response);
				jQuery("#globalLoader").hide();
				jQuery("#shownDbComgates").fadeIn(200);
				
				jQuery("#dbComgateList .dbComgateEditLink").bind("click", comSys.dbComgate.loadDbComgateObject);

			 });
			
		},
			
		
		/**
		 * Loads a dbComgate object and its dbComgateData objects in the
		 * edit mask (via AJAX).
		 */
		loadDbComgateObject : function () {
			
			jQuery("#globalLoader").show();
			jQuery("#dbComgateEdit").fadeOut(200);
			jQuery("#dbComgateList .listItem").removeClass("selected");
			jQuery(this).parentsUntil(".listItem").parent().addClass("selected");
			
			jQuery.get(this.href,{},function(response){ 
			    	
				jQuery("#globalLoader").fadeOut(200, function () {
					jQuery("#dbComgateEdit").css("opacity", "1");
					jQuery("#dbComgateEdit").html("");
					jQuery("#dbComgateEdit").html(response);
					jQuery("#dbComgateEdit").fadeIn(200);	
					comSys.dbComgate.initDbComgateData();
				});
				

			 });
			return false;
		},
		
		/**
		 * Updates the conter of errors in the DbProg.
		 */
		updateNumberOfErrors : function () {
			
			var url = jQuery("#numberOfErrors-reload").attr("href");
			jQuery.get(url,{},function(response){ 
		    	
				jQuery("#numberOfErrors-value").html(response);
			 });
		},
		
		
		/**
		 * Inits a dbComgateData element for editing. 
		 * - Items editable
		 * - Init a dropdown to select section
		 */
		initDbComgateData : function () {
			
			// Make items editable and saveable and addable
			jQuery("#dbComgateDataList .edit").bind("click", comSys.dbComgate.openDbComgateDataEditMask);
			jQuery("#dbComgateDataList .delete").parent().bind("click", comSys.dbComgate.deleteDbComgateData);
			jQuery(".addLocation").bind("click", comSys.dbComgate.loadLinkCityCountryEditor); 
			jQuery("#dbComgateDataList .dbComgateDataSaveLink").bind("click", comSys.dbComgate.saveDbComgateData);
			jQuery("#dbComgateDataList .addDbComgateData").bind("click", comSys.dbComgate.addDbComgateData);
			jQuery("#dbComgateDataList .createDbComgateData").bind("click", comSys.dbComgate.createNewDbComgateData);			
			jQuery("#dbComgateDataList .unUsedsectionSelect").dropdownchecklist({ 
				width				: 200,
				closeRadioOnClick 	: true
			}); 
			
			// Init location create form for City itmes
			
			
			
					
			// Show section dropdown list
			jQuery("#sectionSelect").dropdownchecklist({ 
				width		: 150,
				onItemClick	: comSys.dbComgate.sectionSelected							
			}); 
			
			// Show section dropdown list
			jQuery("#errorSelect").dropdownchecklist({ 
				width		: 150,
				onItemClick	: comSys.dbComgate.errorSelect							
			}); 
			
			// Hide all sections that do not contain errors.
			jQuery("#dbComgateDataList .sectionBox").hide();
			jQuery("#dbComgateDataList .sectionBox").find(".listItem.error").parent().show();

			
			// Init close and save buttons
			jQuery("#closeDbComgate").bind("click", comSys.dbComgate.closeDbComgateDataEditMask);			
			jQuery("#saveDbComgate").bind("click", comSys.dbComgate.saveDbComgate);
			
			// Hide new rows
			jQuery("#dbComgateDataList .listItem.new").hide();
					
		},
		
		/**
		 * Saves the dbComgate object via AJAX call.
		 */
		saveDbComgate : function () {
			
			
			// Prevent multiple save clicks
			if (comSys.dbComgate.saveInProgress == true) return false;
			
			comSys.dbComgate.saveInProgress = true;
			jQuery("#dbComgateEdit").css("opacity", "0.6");
			jQuery("#globalLoader").show();
 			jQuery.get(this.href,{},function(response){ 
 				
 				if (response == "0") {
 					comSys.dbComgate.saveInProgress = false;
 					jQuery("#responseMessage").fadeIn (200);
 					jQuery("#globalLoader").hide();
 					jQuery("#dbComgateEdit").css("opacity", "1.0");
 				} else {
 					comSys.dbComgate.saveInProgress = false;
 					comSys.dbComgate.updateNumberOfErrors();
 					if (comSys.dbComgate.dbComgateSearchDisplayed == true) { // In case e serach list is displayed
 						comSys.dbComgate.removeDbComgateFromSearchList();
 					} else {
 						comSys.dbComgate.loadNextDbComgate();	
 					}
 			    	comSys.dbComgate.closeDbComgateDataEditMask();
 			    	jQuery("#globalLoader").hide();
 				}
 				
 				
			 });
			return false;
		},
		
		
		removeDbComgateFromSearchList : function () {

			var currentDbComgate = jQuery("#currentDbComgate").attr("value");
			jQuery("#dbComgateId_" + currentDbComgate).parentsUntil(".listItem").parent().fadeOut();
		},
		
		
		/**
		 * Saves a dbComgateData object. Thus the original field of the
		 * object is updated via AJAX call.
		 */
		saveDbComgateData : function () {
			
			var listElement = jQuery(this).parentsUntil(".listItem").parent();
			jQuery(listElement).removeClass("error");
			jQuery(listElement).addClass("corrected");
			var newOriginal = jQuery(listElement).find(".newOriginal").attr("value")		
			
			jQuery.get(this.href,{newOriginal : newOriginal},function(response){ 
				
				jQuery(listElement).find(".options").slideToggle();
				jQuery(listElement).removeClass("error");
				jQuery(listElement).addClass("corrected");
				jQuery(listElement).find(".verifiedStatus").first().text("0");
				var newOriginal = jQuery(listElement).find(".newOriginal").first().attr("value");
				jQuery(listElement).find(".original").first().text(newOriginal);		
				
			 });
			return false;
		},
		
		/**
		 * Adds a new dbComgateData element to the given section.
		 */
		addDbComgateData : function () {
				
			var sectionBox = jQuery(this).parentsUntil(".sectionBox").parent();
			jQuery(sectionBox).find(".new").slideToggle ();

			return false;
		},
		
		
		/**
		 * Creates a new dbComgateDataElement via AJAX call.
		 */
		createNewDbComgateData : function () {
			
			var selectedSection 	= jQuery(this).parentsUntil(".listItem").parent().find(".ui-dropdownchecklist-text").attr("title");
			var original			= jQuery(this).parentsUntil(".listItem").parent().find(".newOriginal").first().attr("value");
			var serial				= jQuery(this).parentsUntil(".listItem").parent().find(".newSerial").first().attr("value");
			
			var formIsValid 		= true;
			if(!((parseFloat(serial) == parseInt(serial)) && !isNaN(serial))){				
				comSys.dbComgate.markInputWithInvalidValue(jQuery(this).parentsUntil(".listItem").parent().find(".newSerial").first());
				formIsValid = false;
			}
			
			if (original.length < 1) {
				comSys.dbComgate.markInputWithInvalidValue(jQuery(this).parentsUntil(".listItem").parent().find(".newOriginal").first());
				formIsValid = false;
			}
			
			// Do nothing if the values for serial or original are not valid
			if (formIsValid == false) {
				return false;
			}
				
				
			var listItem 			= jQuery(this).parentsUntil(".listItem").parent();
			jQuery("#globalLoader").show();
			jQuery.get(this.href,{ section: selectedSection, serial : serial, original : original },function(response){ 
				
				// Reset the new list item for next use
				jQuery(listItem).find(".newOriginal").first().attr("value", "");
				jQuery(listItem).find(".newSerial").first().attr("value", serial);
				// Remove the selected section from the dropdown
				jQuery(listItem).find(".unUsedsectionSelect").find("option[value='" + selectedSection + "']").first().remove();//.remove("option[value='" + selectedSection + "']");
				
				var dropDown = jQuery(listItem).find(".unUsedsectionSelect").first();
				
				jQuery(dropDown).dropdownchecklist("destroy");
				
				jQuery(dropDown).dropdownchecklist({ 
					width				: 200,
					closeRadioOnClick 	: true
				}); 
				
			
				if (jQuery(listItem).find(".unUsedsectionSelect").find("option").size() == 0) {
					
					jQuery(listItem).parent().find(".addDbComgateData").hide();
				}
				
				jQuery(listItem).before(response); 
				jQuery(listItem).hide();
				
				jQuery("#dbComgateDataList .delete").parent().unbind();
				jQuery("#dbComgateDataList .delete").parent().bind("click", comSys.dbComgate.deleteDbComgateData);
				
				jQuery("#globalLoader").hide();
			});
			return false;
			
		},
		
		
		markInputWithInvalidValue : function (input) {
			var attr = $(input).attr('type');
			
			if (typeof attr == 'undefined' || attr == false) {
			    return false;
			}
			$(input).effect("highlight", {color : "#F8867E"}, 3000);
		},
		
		
		
		/**
		 * Removes the dbComgate object that is currently edited.
		 * The method is called after save of edited data was successfull.
		 */
		removeCurrentDbComgate : function () {
			$("#dbComgateList .listItem.selected").fadeOut(300);
		},
		
		/**
		 * Is called when a section is selected in the dropdown menu.
		 */
		sectionSelected : function (checkbox, selector) {
			
			var index 	= jQuery(checkbox).attr("index");
			var id		= jQuery(checkbox).attr("value");
			if (index == 0) {
				jQuery("#dbComgateDataList .sectionBox").show();
			} else {
				jQuery("#dbComgateDataList .sectionBox").hide();
				jQuery("#dbComgateDataList #section_" + id).show();
				
			}
		},
		
		
		errorSelect : function (checkbox, selector) { 
			
			var index 	= jQuery(checkbox).attr("index");
			if (index == 1) { // Errors only
			
				jQuery("#dbComgateDataList .sectionBox").hide();
				jQuery("#dbComgateDataList .sectionBox").find(".listItem.error").parent().show();
				
			} else {
				jQuery("#dbComgateDataList .sectionBox").show();				
			}
		},
		
	
		
		/**
		 * Displays an editmask to edit the dbComgateData original field.
		 */
		openDbComgateDataEditMask : function () {
			
			var listItemId = jQuery(this).parentsUntil(".listItem").parent();
			jQuery(listItemId).find(".options").slideToggle(200);
		},
		
		
		deleteDbComgateData : function () {
			
			var url 		= jQuery(this).attr("href");
			var listItem 	= jQuery(this).parentsUntil(".listItem").parent();
			jQuery.get(url, {},function(response){ 

				jQuery(listItem).hide();
			});
			
			return false;
		},
		
		/**
		 * Colosing the DbComgate edit form
		 */
		closeDbComgateDataEditMask : function () {
			
			jQuery("#dbComgateEdit").fadeOut(200);
			
		},
		
		/**
		 * Creates a new linkCityCountry object.
		 */
		createLinkCityCountry : function () {
			
		
			var form			= jQuery(this).parent();
			var zip				= jQuery(form).find(".zip").first().attr("value");
			var city			= jQuery(form).find(".city").first().attr("value");;
			var county			= jQuery(form).find(".county").first().attr("value");;
			var countryCode		= jQuery(form).find(".countryCode").first().attr("value");;
			var sourceUnique	= jQuery(form).find(".sourceUnique").first().attr("value");;
			var cityOriginal	= jQuery(form).find(".original").first().attr("value");;
			var countryCodeId 	= jQuery(form).find(".countryCodeId").first().attr("value");;
			
			var formData		= jQuery(this).parent().serialize();
 			var url				= jQuery(form).attr("action");
 			var listItem 		= jQuery(form).parentsUntil(".listItem").parent();
			
 			
 			if (city.length < 1) {
 				jQuery(form).find(".message").addClass("error");
 				jQuery(form).find(".message").text('Error: The value for the field "City correct" has to be set.');
 				return false;
 			}
 			jQuery("#globalLoader").show();
			jQuery.ajax({
				  
				type: 'POST',
				url: url,
				data: formData,
				success: function (data) {
				
						var linkCityCountryForm = jQuery(form).parentsUntil(".linkCityCountryCreate").parent();
						jQuery(linkCityCountryForm).find(".linkCityCountryList").html(data);
						comSys.dbComgate.initLinkCityCountryList(linkCityCountryForm);	
						jQuery(linkCityCountryForm).find(".linkCityCountryForm").slideToggle();
						jQuery("#globalLoader").hide();
				  }
			});
			
		},
		
		/**
		 * Updates a list item of a dbComgateData element. Does AJAX call
		 * to fetch new version and replaces in DOM
		 */
		reloadDbComgateDataListEntry : function (dbComgateDataId, markAsCorrected, section) {
			
			if (parseInt(dbComgateDataId) < 1) return false;
			
			
			var elementToReload = jQuery("#listItem_" + dbComgateDataId).first();
			
			if (jQuery(elementToReload).length == 1) {
				var url = jQuery(".countryCodeIdLink").first().attr("href") + "?dbComgateDataId=" + 	dbComgateDataId;
				
				jQuery.get(url, {},function(response){ 
					
					$(elementToReload).replaceWith(response);
					
					var updatedElement = jQuery("#listItem_" + dbComgateDataId);
					if (markAsCorrected) {
						jQuery(updatedElement).addClass("corrected");
					}
					jQuery(updatedElement).find(".edit").bind("click", comSys.dbComgate.openDbComgateDataEditMask);
					jQuery(updatedElement).find(".delete").bind("click", comSys.dbComgate.deleteDbComgateData);
					jQuery(updatedElement).find(".addLocation").bind("click", comSys.dbComgate.loadLinkCityCountryEditor); 
				});
			} else {
				var url = jQuery(".countryCodeIdLink").first().attr("href") + "?dbComgateDataId=" + 	dbComgateDataId;
				
				jQuery.get(url, {},function(response){ 
						
					jQuery("#" + section).find(".listItem").last().after(response);
					
					var updatedElement = jQuery("#listItem_" + dbComgateDataId);
					if (markAsCorrected) {
						jQuery(updatedElement).addClass("corrected");
					}
					jQuery(updatedElement).find(".edit").bind("click", comSys.dbComgate.openDbComgateDataEditMask);
					jQuery(updatedElement).find(".delete").bind("click", comSys.dbComgate.deleteDbComgateData);
					jQuery(updatedElement).find(".addLocation").bind("click", comSys.dbComgate.loadLinkCityCountryEditor); 
				});
			}
			
			
		},
		
		/**
		 * Reads a list of LinkCityCounry elements from the server and displays that list.
		 * It also shows a form to create new LinkCityCountry elements.
		 */
		loadLinkCityCountryEditor : function () {
			
			jQuery("#globalLoader").show();
			jQuery(this).parentsUntil(".listItem").parent().find(".linkCityCountryCreate").first().slideToggle();
			

			var linkCityCountryForm = jQuery(this).parentsUntil(".listItem").parent().find(".linkCityCountryCreate").first();
			
			var url = jQuery(linkCityCountryForm).find(".linkCityCountryListLink").first().attr("href");  
			// Load the link city country list
			jQuery.get(url, {},function(response){ 
				
			
				jQuery(linkCityCountryForm).find(".linkCityCountryList").html(response);

				
				jQuery(linkCityCountryForm).find(".linkCityCountryControls").find(".saveLinkCityCountry").bind("click", comSys.dbComgate.selectLinkCityCountryFromList);
				jQuery(linkCityCountryForm).find(".addLinkCityCountry").bind("click", function () {
					jQuery(linkCityCountryForm).find(".linkCityCountryForm").first().slideToggle();
				});
				
				comSys.dbComgate.initLinkCityCountryList(linkCityCountryForm);	
				
				jQuery(linkCityCountryForm).find(".saveNewCityLinkList").bind("click", comSys.dbComgate.createLinkCityCountry);
				jQuery("#globalLoader").hide();
			});	
		},
		
		
		searchLinkCityCountryList : function (button) {
			
			var searchForm 		= jQuery(button).parentsUntil(".searchLinkCityCountryListForm").parent();
			var url 			= jQuery(searchForm).attr("action");
			var searchCity 		= jQuery(searchForm).find(".citySearch").attr("value");
			var countryCode 	= jQuery(searchForm).find(".countrySearch").attr("value");
		
			if (searchCity.length < 3) return false;
			
			
			jQuery.get(url, {cityName : searchCity, countryCode : countryCode}, function(response){ 
				
				var linkCityCountryForm = jQuery(searchForm).parentsUntil(".linkCityCountryCreate").parent();
	
				jQuery(linkCityCountryForm).find(".linkCityCountryList").html(response);
				comSys.dbComgate.initLinkCityCountryList(linkCityCountryForm);	
			
			});
			
		},
		
		initLinkCityCountryList : function (linkCityCountryForm) {
			
			// Country drop box for country list search
			jQuery(linkCityCountryForm).find(".linkCityCountryList .countrySearch").dropdownchecklist({ 
				width				: 120,
				closeRadioOnClick 	: true,
				maxDropHeight		: 150 
			}); 
			
			jQuery(linkCityCountryForm).find(".linkCityCountryList").find(".select").bind("click", function () {
				
				jQuery(linkCityCountryForm).find(".linkCityCountryList").find(".linkCityCountryItem .select").find(".check").attr('checked', false);
				jQuery(linkCityCountryForm).find(".linkCityCountryList").find(".linkCityCountryItem").removeClass("selected");
				jQuery(this).find(".check").attr('checked', true);
				jQuery(this).parent().parent().addClass("selected");
				
			});
			
			// Search country list event
			jQuery(linkCityCountryForm).find(".searchButton").bind("click", function() {
				comSys.dbComgate.searchLinkCityCountryList(this);
			});
			
			
			jQuery(linkCityCountryForm).find("input[name='city']").keypress(function (event) {
				 if ( event.which == 13 ) {
					 var button = jQuery(linkCityCountryForm).find(".searchButton");
					 comSys.dbComgate.searchLinkCityCountryList(button);
					 return false;
				 }
			});
			
			jQuery(linkCityCountryForm).find(".linkCityCountryList").find("input[type='checkbox'][checked=checked]").parentsUntil(".linkCityCountryItem").parent().addClass("selected");
		},
		
		
		/**
		 * For a given city DbComgateData element a element out of the linkCityList is selected.
		 * 
		 */
		selectLinkCityCountryFromList : function () {
					
			
			// The whole dom representing country list and form
			var linkCityCountryForm 	= jQuery(this).parentsUntil(".listItem").parent().find(".linkCityCountryCreate").first();
			// The selected country link 
			var selectedLinkCityCountry = jQuery(linkCityCountryForm).find(".linkCityCountryList .linkCityCountryItem.selected");
			var sectionId				= jQuery(linkCityCountryForm).parentsUntil(".sectionBox").parent().attr("id");
			
	
			// Do nothing if no link city country is selected
			if (jQuery(selectedLinkCityCountry).size() == 0) {
				return false;
			}
			
			jQuery(linkCityCountryForm).find(".linkCityCountryList").fadeTo( "fast", 0.6 );
			jQuery("#globalLoader").show();

			var url = jQuery(selectedLinkCityCountry).find(".selectLinkCityCountryLink").attr("href");
			
			jQuery.get(url, {},function(response){ 
				
				var answer = jQuery.parseJSON(response);
				
				for(var i=0;i<answer.length;i++){
			        var dbComgateDataId = answer[i];
			        comSys.dbComgate.reloadDbComgateDataListEntry(dbComgateDataId, true, sectionId);
			    }	
				jQuery(linkCityCountryForm).find(".linkCityCountryList").fadeTo( "fast", 1.0 );
				jQuery("#globalLoader").hide();
			});	
			
		}	
			
	}				
}


