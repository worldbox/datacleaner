<?php

namespace Worldbox\ComSys\Log;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */


use TYPO3\FLOW3\Annotations as FLOW3;


class ComSysLogger  {
	
	const SEVERITY_NOTICE 	= 0;
	const SEVERITY_WARNING	= 1;
	const SEVERITY_ERROR 	= 2;
	
	private static $logger = null;
	
	private $microtTime 	= null;
	
	public function __construct () {
		
	}
	
	public static function getLogger () {
		if (self::$logger == null) {
			self::$logger = new ComSysLogger();
		}
		
		return self::$logger;
	}
	
	public function log ($message, $severity = self::SEVERITY_NOTICE) {
		
		if ($severity == self::SEVERITY_NOTICE) {
			$prefix = "Notice: ";
		} else if ($severity == self::SEVERITY_WARNING) {
			$prefix = "Warning: ";
		} else {
			$prefix = "Error: ";
		}
		
		$message = "---------------------------------------\n". $prefix . $message."\n";
		if (is_dir("/serverdata/worldboxglobal.ch/stage.comgate/html/comsys/Data/Logs/")) {
			if ($_SERVER['HTTP_HOST'] == "comgate.worldboxglobal.ch") {
				file_put_contents("/serverdata/worldboxglobal.ch/comgate/html/comsys/Data/Logs/ComSys.log", $message, FILE_APPEND);
			} else {
				file_put_contents("/serverdata/worldboxglobal.ch/stage.comgate/html/comsys/Data/Logs/ComSys.log", $message, FILE_APPEND);
			}
			
		} else if (is_dir("/var/www/DataCleaner/Data/Logs/")) {
			file_put_contents("/var/www/DataCleaner/Data/Logs/ComSys.log", $message, FILE_APPEND);
		}
		
		
	}
	
	
	public function logTime ($msg = "") {
		
		if ($this->microtTime == null) {
			$this->microtTime = microtime(true);
		}
		$now 		= 	microtime(true);
		$elapsed 	=   $now - $this->microtTime;
		$message    = "Elapsed Time " . $elapsed . " - " . $msg . "\n";
		if (is_dir("/serverdata/worldboxglobal.ch/stage.comgate/html/comsys/Data/Logs/")) {
			file_put_contents("/serverdata/worldboxglobal.ch/stage.comgate/html/comsys/Data/Logs/ComSysTime.log", $message, FILE_APPEND);
		}
	}
	
}