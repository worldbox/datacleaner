<?php
namespace Worldbox\ComSys\Domain\Repository;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */

use Worldbox\ComSys\Log\ComSysLogger;

use TYPO3\FLOW3\Annotations as FLOW3;
use TYPO3\FLOW3\Persistence\QueryInterface;
use TYPO3\FLOW3\Core\Bootstrap;
/**
 * A repository for DbComgates
 *
 * @FLOW3\Scope("singleton")
 */
class DbComgateRepository extends \TYPO3\FLOW3\Persistence\Repository {

	
	/**
	 * @FLOW3\Inject
	 * @var Worldbox\ComSys\Log\ComSysLogger
	 */
	protected $comSysLogger;
	
	private static $BATCH_SIZE = 10;
	
	/**
	 * 
	 * @param int $id
	 */
	public function getSingle () {

		$query = $this->createQuery();
		$result = $query->setLimit(1)->execute();
        return $result->getFirst();
	}
	
	
	/**
	 * Returns the next batch of DbComgate objects of a given program.
	 *
	 * @param int $progCode
	 */
	public function getNextBetch ($progCode) {
		
		$query = $this->createQuery();
		$query->setLimit(self::$BATCH_SIZE);
		
			
		$constraints 	= array();
		$constraints[] 	= $query->equals("progCode",$progCode);
		$constraints[] 	= $query->equals("verifiedStatus", 1);
		//$constraints[] 	= $query->logicalNot($query->equals("verifiedDate", null));
				
		$query->matching($query->logicalAnd($constraints));
		

		$query->setOrderings(array("verifiedDate" => QueryInterface::ORDER_ASCENDING, "theid" => QueryInterface::ORDER_ASCENDING));
		
		
		$result = $query->execute();
		
		
		return $result;
	}
	
	/**
	 * Returns the latest db comgate (oldest) element. 
	 * 
	 * @param int $progCode
	 */
	public function getCurrentDbComgate ($progCode) {
		
		$query = $this->createQuery();
		$query->setLimit(1);	
		
		$constraints 	= array();
		$constraints[] 	= $query->equals("progCode",$progCode);
		$constraints[] 	= $query->equals("verifiedStatus", 1);
				
		$query->matching($query->logicalAnd($constraints));
		
		$query->setOrderings(array("verifiedDate" => QueryInterface::ORDER_ASCENDING, "theid" => QueryInterface::ORDER_ASCENDING));
		
		$result = $query->execute();
		return $result;
	}
	
	
	
	/**
	 * Returns the DbComgate object, that follows $dbComgate. Its the one with
	 * verifedDate equal or greater than $dbComgate and theid > $dbComgate->theid
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 * @param int $progCode
	 */
	public function getNext (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate, $progCode) {
		
		$query = $this->createQuery();
		$query->setLimit(1);
		
		$constraints 	= array();
		$constraints[] 	= $query->equals("verifiedStatus", 1);
		$constraints[] 	= $query->equals("progCode", $progCode);
		$constraints[] 	= $query->logicalNot($query->equals("theid", $dbComgate->getTheid()));
		
		if ($dbComgate->getVerifiedDate() != null) {
			$constraints[] 	= $query->greaterThanOrEqual("verifiedDate", $dbComgate->getVerifiedDate());
		}
		
		$query->setOrderings(array("verifiedDate" => QueryInterface::ORDER_ASCENDING, "theid" => QueryInterface::ORDER_ASCENDING));
		$constraints[] 	= $query->greaterThanOrEqual("theid", $dbComgate->getTheid());
		$query->matching($query->logicalAnd($constraints));
		$result = $query->execute();
		if ($result->count() == 0) {
			return false;
		}
		
		return $result->getFirst();
	}
	
	/**
	 * Returns the DbComgate object, that follows $dbComgate. Its the one with
	 * verifedDate equal or greater than $dbComgate and theid > $dbComgate->theid
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 * @param int $progCode
	 */
	public function getPrevious (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate, $progCode) {
		
		$query = $this->createQuery();
		$query->setLimit(1);
		
		$constraints 	= array();
		$constraints[] 	= $query->equals("verifiedStatus", 1);
		$constraints[] 	= $query->equals("progCode", $progCode);
		$constraints[] 	= $query->logicalNot($query->equals("theid", $dbComgate->getTheid()));
		
		if ($dbComgate->getVerifiedDate() != null) {
			$constraints[] 	= $query->lessThanOrEqual("verifiedDate", $dbComgate->getVerifiedDate());
		}
		$constraints[] 	= $query->lessThanOrEqual("theid", $dbComgate->getTheid());
		$query->setOrderings(array("verifiedDate" => QueryInterface::ORDER_DESCENDING, "theid" => QueryInterface::ORDER_DESCENDING));
		$query->matching($query->logicalAnd($constraints));
		$result = $query->execute();
		if ($result->count() == 0) {
			return false;
		}
		
		return $result->getFirst();
	}
	
	
	
	
	/**
	 * Returns the DbComgate object with url containing $searchString
	 * 
	 * @param string $searchString
	 * @param int $progCode
	 */
	public function searchByUrl ($progCode, $searchString) {
		
		$query = $this->createQuery();
		$query->setLimit(self::$BATCH_SIZE);
		
			
		$constraints 	= array();
		$constraints[] 	= $query->equals("progCode", $progCode);
		$constraints[] 	= $query->equals("verifiedStatus", 1);
		$constraints[] 	= $query->like("url", "%" . $searchString . "%");
				
		$query->matching($query->logicalAnd($constraints));
		
		$query->setOrderings(array("verifiedDate" => QueryInterface::ORDER_ASCENDING, "theid" => QueryInterface::ORDER_ASCENDING));
		
		$result = $query->execute();
				
		return $result;
	}
	
	
	public function getFirstNoWin ($progCode) {
		$query = $this->createQuery();
		$query->setLimit(1);
		
		$constraints[] 	= $query->equals("verifiedStatus", 5);
		$constraints[] 	= $query->equals("progCode", $progCode);
		$query->matching($query->logicalAnd($constraints));
		
		$query->setOrderings(array("theid" => QueryInterface::ORDER_ASCENDING));
		$result = $query->execute();
		
		return $result->getFirst();
	}
	
	
	public function getNextNoWin (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate, $progCode) {
		
		$query = $this->createQuery();
		$query->setLimit(1);
		
		$constraints 	= array();
		$constraints[] 	= $query->equals("verifiedStatus", 5);
		$constraints[] 	= $query->equals("progCode", $progCode);
		$constraints[] 	= $query->greaterThan("theid", $dbComgate->getTheid());
		
		$query->setOrderings(array("theid" => QueryInterface::ORDER_ASCENDING));
		$query->matching($query->logicalAnd($constraints));
		$result = $query->execute();
	
		return $result->getFirst();
	}

	
	public function getPreviousNoWin (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate, $progCode) {
	
		$query = $this->createQuery();
		$query->setLimit(1);
	
		$constraints 	= array();
		$constraints[] 	= $query->equals("verifiedStatus", 5);
		$constraints[] 	= $query->equals("progCode", $progCode);
		$constraints[] 	= $query->logicalNot($query->equals("theid", $dbComgate->getTheid()));
		$constraints[] 	= $query->lessThan("theid", $dbComgate->getTheid());
	
		$query->setOrderings(array("theid" => QueryInterface::ORDER_DESCENDING));
		$query->matching($query->logicalAnd($constraints));
		$result = $query->execute();
		
		return $result->getFirst();
	}
	
	/**
	 * Returns the number of dbComgate of the given prog that have status 5.
	 * 
	 * @param string $progCode
	 */
	public function getNumberOfMatchCasesNoWin ($progCode) {
		
		$conn 		= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection();
		
		$rawQuery 	= "SELECT COUNT(*) FROM DB_COMGATE WHERE VERIFIED_STATUS = 5 AND PROG_CODE = " . $progCode;
		$result 	= $conn->executeQuery($rawQuery)->fetchAll();;
		return $result;
	}
}
?>