<?php
namespace Worldbox\ComSys\Domain\Repository;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;
use TYPO3\FLOW3\Core\Bootstrap;

/**
 * A repository for DbComgateDatas
 *
 * @FLOW3\Scope("singleton")
 */
class DbComgateDataRepository extends \TYPO3\FLOW3\Persistence\Repository {

	// add customized methods here
	
	public function manualInsert () {
		
		$conn 				= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection(); 
		$query = "INSERT INTO DB_COMGATE_DATA () VALUES ()";	

	
		$result 	= $conn->executeQuery($query);
		return $conn->lastInsertId();
	}
}
?>