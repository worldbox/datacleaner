<?php
namespace Worldbox\ComSys\Domain\Repository;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */

use Worldbox\ComSys\Log\ComSysLogger;
use TYPO3\FLOW3\Core\Bootstrap;
use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * A repository for DbProgs
 *
 * @FLOW3\Scope("singleton")
 */
class DbProgRepository extends \TYPO3\FLOW3\Persistence\Repository {

	// add customized methods here
	
	/**
	 * Returs the number of "open" WIN map cases.
	 * 
	 * @return int
	 */
	public function getNumberOfOpenWinMapCases (\Worldbox\ComSys\Domain\Model\DbProg $dbProg) {
			

		$conn 		= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection(); 
		
		$rawQuery = "SELECT COUNT( DISTINCT dc.theid ) AS count
					FROM 
						DB_COMGATE AS dc
					JOIN 
						DB_COMGATE_COMP AS dcc
					ON 
						dc.THEID=dcc.COMGATE_ID
					WHERE 
						dc.prog_code = " . $dbProg->getProgCode() ."
					AND 
						dcc.match_status =5	
					AND 
						dcc.verified_status = 9";

		$result 			= $conn->executeQuery($rawQuery)->fetchAll();
		return $result[0]['count'];
	}
	
	
	/**
	 * Returns all dbComgate objects of a program that have a db_comgate_compt
	 * with match_status = 5
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 */
	public function getDbComgateCases (\Worldbox\ComSys\Domain\Model\DbProg $dbProg) {
		
		$conn 		= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection(); 
		
		$rawQuery = "SELECT DISTINCT
					    dc.theid AS THEID
					FROM 
						DB_COMGATE AS dc
					JOIN 
						DB_COMGATE_COMP AS dcc
					ON 
						dc.THEID=dcc.COMGATE_ID
					WHERE 
						dc.prog_code = " . $dbProg->getProgCode() ."
					AND 
						dcc.match_status =5	
					AND 
						dcc.verified_status = 9
					LIMIT 0, 100";
		
	
		$result 			= $conn->executeQuery($rawQuery)->fetchAll();
		return $result;
	}
	

	/**
	 * Returns all dbProgs that have a db_comgate with no WIN (verified_status=5)
	 */
	public function getDbProgNoWinCases(){
		
		$conn 		= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection();
		
		$rawQuery 	= "SELECT * 
						FROM DB_PROG p
						WHERE EXISTS
						(
						   SELECT 1 FROM DB_COMGATE c
						   WHERE c.prog_code = p.prog_code
						   AND c.verified_status = 5
						)";
		
		$result 	= $conn->executeQuery($rawQuery)->fetchAll();
		return $result;
		
	}
	
	
}
?>