<?php
namespace Worldbox\ComSys\Domain\Repository;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */

use Worldbox\ComSys\Log\ComSysLogger;

use TYPO3\FLOW3\Annotations as FLOW3;
use TYPO3\FLOW3\Persistence\QueryInterface;

/**
 * A repository for LinkProgSection
 *
 * @FLOW3\Scope("singleton")
 */
class LinkProgSectionRepository extends \TYPO3\FLOW3\Persistence\Repository {

	
	public function getWinMatchCompLinkProgSections () {
		
		$query = $this->createQuery();
		$query->matching($query->equals("sectionCode", "SHAR"));
		return$query->execute();
		
	}
	
}
?>