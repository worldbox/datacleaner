<?php

namespace Worldbox\ComSys\Domain\Repository;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */
use Worldbox\ComSys\Log\ComSysLogger;

use TYPO3\FLOW3\Annotations as FLOW3;
use TYPO3\FLOW3\Core\Bootstrap;

/**
 * A repository for DbProgs
 *
 * @FLOW3\Scope("singleton")
 */
class LinkCityCountryRepository extends \TYPO3\FLOW3\Persistence\Repository {

	// add customized methods here
	
	public function getCountryList () {
		$conn 				= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection(); 
		$rawQuery 			= "SELECT country_code, text_de, text_en FROM TAB_COUNTRY ORDER BY text_en";
		$result 			= $conn->executeQuery($rawQuery)->fetchAll();
		
		$countries 			= array();
		foreach ($result as $row) {
			$countries[] 	= $row;
		}
		return $countries;
	}
	
	/**
	 * Returns the LinkCityCountry Object that has the given $sourceCity and $countryCode
	 * @param string $countryCode
	 * @param string $sourceCity
	 */
	public function getLinkCityCountry ($sourceCity, $countryCode = false) {
		
		$query = $this->createQuery();
		$query->setLimit(1);
		
			
		$constraints 	= array();
			
		$constraints[] 	= $query->like("sourceCity", $sourceCity);
		if ($countryCode  != false) {
			$constraints[] 	= $query->like("countryCode", $countryCode);
		}
		
		
				
		$query->matching($query->logicalAnd($constraints));
		
		$result = $query->execute();
		
		if ($result->count() == 0) {
			return null;
		}
		
		return $result->getFirst();
	}
	
	/**
	 * Returns a list that matches the given $city and $countryCode-
	 * 
	 * @param unknown_type $city
	 * @param unknown_type $countryCode
	 */
	public function getLinkCityCountryList ($city, $countryCode) {
		
		$query = $this->createQuery();
		
		$constraints 	= array();
		$citySub 		= strtoupper(substr($city, 0, 3));
			
		$constraints[] 	= $query->like("sourceCityUc", $citySub . "%");
		if (strlen($countryCode) > 0) {
			$constraints[] 	= $query->equals("countryCode", $countryCode);
		}
		
		
		$query->matching($query->logicalAnd($constraints));
		
		$result = $query->execute();
		
		return $result->toArray();
	}
	
	public function getByCountryCode ($countryCode) {
		
		$query 			= $this->createQuery();

		$query->matching($query->equals("countryCode", $countryCode));
		
		$result = $query->execute();
		ComSysLogger::getLogger()->log(count($result));
		return $result->getFirst();
	}

}
?>