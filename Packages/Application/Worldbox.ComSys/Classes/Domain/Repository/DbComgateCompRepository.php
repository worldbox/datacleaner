<?php
namespace Worldbox\ComSys\Domain\Repository;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */

use Worldbox\ComSys\Log\ComSysLogger;

use TYPO3\FLOW3\Annotations as FLOW3;
use TYPO3\FLOW3\Core\Bootstrap;

/**
 * A repository for DbComgateDatas
 *
 * @FLOW3\Scope("singleton")
 */
class DbComgateCompRepository extends \TYPO3\FLOW3\Persistence\Repository {


	
	/**
	 * Just for developement purpose to reset test data to default.
	 */
	public function restoreTestData () {
		$conn 				= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection(); 
		$query				 = "UPDATE DB_COMGATE_COMP SET match_status=5, win = '' WHERE theid IN (588, 589, 590, 591, 592)";
		$conn->executeQuery($query);
	}
	
	/**
	 * Given a dbProg the method returns a dbComgateComp object. If $dbComgateCompId is
	 * set it returns the one with the next higher theid. Otherwise the one with the lowest id.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 * @param unknown_type $dbComgateCompId
	 */
	public function getNextDbComgateCompByParentCompany (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate, $dbComgateCompId = null) {
		
		$conn 				= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection(); 
		$additionalWhere 	= "";
		if ($dbComgateCompId != null && (int) $dbComgateCompId > 0) {
			$additionalWhere = "AND db_comgate_comp.theid > " . $dbComgateCompId;
		} 
		$rawQuery	= 	"SELECT 
							*
						FROM 
							DB_COMGATE_COMP as db_comgate_comp
						WHERE 
							match_status =5
							AND 
							comgate_id = " . $dbComgate->getTheid() . "
							" . $additionalWhere . "	
						ORDER BY theid asc LIMIT 1	
							";
		
		$result 	= $conn->executeQuery($rawQuery)->fetchAll();
		ComSysLogger::getLogger()->log($rawQuery);
		ComSysLogger::getLogger()->log(print_r($result, true));
		
		if (count($result) == 0) {
			return null;
		}
		
		return $this->findByIdentifier($result[0]['THEID']);
	}
	
	/**
	 * Given a dbProg the method returns a dbComgateComp object. If $dbComgateCompId is
	 * set it returns the one with the next lower theid. Otherwise the one with the lowest id.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 * @param unknown_type $dbComgateCompId
	 */
	public function getPreviousDbComgateCompByParentCompany (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate, $dbComgateCompId = null) {
		
		$conn 				= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection(); 
		$additionalWhere 	= "";
		if ($dbComgateCompId != null && (int) $dbComgateCompId > 0) {
			$additionalWhere = "AND db_comgate_comp.theid < " . $dbComgateCompId;
		} 
		$rawQuery	= 	"SELECT 
							*
						FROM 
							DB_COMGATE_COMP as db_comgate_comp
						WHERE 
							match_status =5
							AND 
							comgate_id = " . $dbComgate->getTheid() . "
						" . $additionalWhere . "	
						ORDER BY theid desc LIMIT 1	
							";
		
		$result 	= $conn->executeQuery($rawQuery)->fetchAll();
		if (count($result) == 0) {
			return null;
		}
		
		return $this->findByIdentifier($result[0]['THEID']);
	}
	
	
	/**
	 * Returns the number of cases for that $dbComgate (thus dbComgateComp with status 5)
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 */
	public function getNumberOfCasesByDbComgate (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate) {
		
		$conn 		= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection(); 
		$rawQuery	= 	"SELECT 
							COUNT(*) as numberOfCases
						FROM 
							DB_COMGATE_COMP as db_comgate_comp
						WHERE 
							match_status =5
							AND 
							comgate_id = " . $dbComgate->getTheid();
		
		$result 	= $conn->executeQuery($rawQuery)->fetchAll();
		return $result[0]['numberOfCases'];
		
	}
	
	
	/**
	 * Returns the number of cases for that $dbComgate (thus dbComgateComp with status 5)
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 */
	public function getNumberOfCasesByCountry ($progCode, $countryCode) {
		
		$conn 		= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection(); 
		$rawQuery	= 	"SELECT 
							COUNT(*) as numberOfCases
						FROM 
							DB_COMGATE_COMP  
						JOIN 
							DB_COMGATE_DATA 
						ON 
							DB_COMGATE_DATA.comgate_id_comp = DB_COMGATE_COMP.theid 
						JOIN 
							DB_COMGATE ON DB_COMGATE.theid = DB_COMGATE_COMP.COMGATE_ID 
						WHERE 
							DB_COMGATE_COMP.match_status = 5 
							AND 
							DB_COMGATE_DATA.dataelement = 'Ident/AddressCountry_code' 
							AND 
							DB_COMGATE_DATA.verified_token = '" . $countryCode . "' 
							AND 
							DB_COMGATE.prog_code = " . $progCode . " 
						ORDER BY 
							DB_COMGATE_COMP.theid ASC
						LIMIT 1
						";
		
		$result 	= $conn->executeQuery($rawQuery)->fetchAll();
		return $result[0]['numberOfCases'];
		
	}
	
	
	
	/**
	 * The method returns the next dbComgateComp object.
	 * - Has to have dataelement matching country code
	 * - In given prog
	 * - theid > than $dbComgateCompId if given
	 */
	public function getNextDbComgateCompByCountry ($progCode, $countryCode, $dbComgateCompId = null) {
		
		$additionalWhere 	= "";
		if ($dbComgateCompId != null && (int) $dbComgateCompId > 0) {
			$additionalWhere = " AND DB_COMGATE_COMP.theid > " . $dbComgateCompId;
		} 
		
		if ($countryCode == "ALL") {
			$countryWhere = " ";
		} else {
			$countryWhere = "DB_COMGATE_DATA.verified_token = '" . $countryCode . "' AND";
		}
		
		$conn 				= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection(); 

		$rawQuery	= 	"SELECT 
							DB_COMGATE_COMP.* 
						FROM 
							DB_COMGATE_COMP  
						JOIN 
							DB_COMGATE_DATA 
						ON 
							DB_COMGATE_DATA.comgate_id_comp = DB_COMGATE_COMP.theid 
						JOIN 
							DB_COMGATE ON DB_COMGATE.theid = DB_COMGATE_COMP.COMGATE_ID 
						WHERE 
							DB_COMGATE_COMP.match_status = 5 
							AND 
							DB_COMGATE_DATA.dataelement = 'Ident/AddressCountry_code' 
							AND 
							" . $countryWhere . "
							DB_COMGATE.prog_code = " . $progCode . " 
							" . $additionalWhere . "
						ORDER BY 
							DB_COMGATE_COMP.theid ASC
						LIMIT 1
						";
		ComSysLogger::getLogger()->log($rawQuery);
		$result 	= $conn->executeQuery($rawQuery)->fetchAll();
		if (count($result) == 0) {
			return null;
		}
		
		return $this->findByIdentifier($result[0]['THEID']);
	}
	
	public function getPreviousDbComgateCompByCountry ($progCode, $countryCode, $dbComgateCompId = null) {
		
		$additionalWhere 	= "";
		if ($dbComgateCompId != null && (int) $dbComgateCompId > 0) {
			$additionalWhere = " AND DB_COMGATE_COMP.theid < " . $dbComgateCompId;
		} 
		
		$conn 				= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection(); 

		$rawQuery	= 	"SELECT 
						DB_COMGATE_COMP.* 
					FROM 
						DB_COMGATE_COMP  
					JOIN 
						DB_COMGATE_DATA 
					ON 
						DB_COMGATE_DATA.comgate_id_comp = DB_COMGATE_COMP.theid 
					JOIN 
						DB_COMGATE ON DB_COMGATE.theid = DB_COMGATE_COMP.COMGATE_ID 
					WHERE 
						DB_COMGATE_COMP.match_status = 5 
						AND 
						DB_COMGATE_DATA.dataelement = 'Ident/AddressCountry_code' 
						AND 
						DB_COMGATE_DATA.verified_token = '" . $countryCode . "' 
						AND 
						DB_COMGATE.prog_code = " . $progCode . " 
						" . $additionalWhere . "
					ORDER BY 
						DB_COMGATE_COMP.theid DESC
					LIMIT 1
					";
		
		$result 	= $conn->executeQuery($rawQuery)->fetchAll();
		if (count($result) == 0) {
			return null;
		}
		
		return $this->findByIdentifier($result[0]['THEID']);
	}
	
	
	
	/**
	 * !! THINK PROG NOT NEEDED
	 * .
	 * Returns all DB_COMGATE_COMP records having a DB_COMGATE_DATA record
	 * with DATAELEMENT = 'Ident/AddressCountry_code' and ORIGINAL = $countryCode. And additionally
	 * match_status=5 and db_prog = $dbProg
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 * @param string $countryCode
	 */
	public function getByProgAndCountryCode (\Worldbox\ComSys\Domain\Model\DbProg $dbProg, $countryCode) {
		
		$conn 				= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection(); 
		$countryCode 		= "CHN";
		//@todo: check for prog code
		$rawQuery	= 	"SELECT 
							*
						FROM 
							DB_COMGATE_COMP as db_comgate_comp
						WHERE 
							match_status =5
							AND
							theid
							IN (
								SELECT 
									comgate_id_comp
								FROM 
									DB_COMGATE_DATA
								WHERE 
									dataelement ='Ident/Coutry_code'
									AND
									original = '" . $countryCode . "'
							)
						";
		
		ComSysLogger::getLogger()->log($rawQuery);
		$result 	= $conn->executeQuery($rawQuery)->fetchAll();
		if (count($result) == 0) {
			return array();
		}
		$dbComgateCompElements = array();
		foreach ($result as $res) {
			$dbComgateCompElements[] = $this->findByIdentifier($res['THEID']);
		}
		
		return $dbComgateCompElements;		
	}

}
















?>