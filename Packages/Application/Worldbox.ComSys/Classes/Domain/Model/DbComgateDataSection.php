<?php
namespace Worldbox\ComSys\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */

use Worldbox\ComSys\Log\ComSysLogger;

use TYPO3\FLOW3\Annotations as FLOW3;
use Doctrine\ORM\Mapping as ORM;
use Worldbox\ComSys\Domain\Repository;

/**
 * A Db comgate data
 *
 * @FLOW3\Entity
 * @ORM\Table(name="DB_COMGATE_DATA")
 */
class DbComgateDataSection {
	
	/**
	 * @FLOW3\Transient
	 */
	private $name;
	
	/**
	 * @FLOW3\Transient
	 */
	private $serial;
	
	/**
	 * States whether there is a sction with the same name
	 * @FLOW3\Transient
	 */
	private $sectionWithSameNameExists;
	
	/**
	 * @FLOW3\Transient
	 */
	private $usedSectionElements;
	
	/**
	 * @FLOW3\Transient
	 */
	private $notUsedSectionElements;
	
	
	
	public function getName () {
		return $this->name;
	}

	public function setName ($name) {
		$this->name = $name;
	}
	
	public function getSerial() {
		return $this->serial;
	}

	public function setSerial ($serial) {
		$this->serial = $serial;
	}
	
	public function getUsedSectionElements () {
		return $this->usedSectionElements;
	}
	
	/**
	 * 
	 * @param array $usedSectionElements
	 */
	public function setUsedSectionElements ($usedSectionElements) {
		$this->usedSectionElements = $usedSectionElements;
	}
	
	
	public function getNotUsedSectionElements () {
		return $this->notUsedSectionElements;
	}
	
	/**
	 * 
	 * @param array $notUsedSectionElements
	 */
	public function setNotUsedSectionElements ($notUsedSectionElements) {
		$this->notUsedSectionElements = $notUsedSectionElements;
	}
	
	/**
	 * Returns the name how the section is dispayed. If there are more sections
	 * with the same name the serial is appended to the name.
	 * 
	 * @return string
	 */
	public function getScreenName () {
		if ($this->sectionWithSameNameExists) {
			return $this->getName() . " " . $this->getSerial();
		}
		return $this->getName(); 
	}
	
	/**
	 * Notifys the object, that there is a sction with same name.
	 */
	public function setSectionWithSameNameExists ($sectionWithSameNameExists) {
		$this->sectionWithSameNameExists = $sectionWithSameNameExists;
	}
	
	
	/**
	 * Returns true if there are unused section elemements.
	 * @return boolean
	 */
	public function getHasNotUsedSections () {
		if (count($this->getNotUsedSectionElements()) > 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * Returns true if there is a DbComgateData element in the section out of:
	 * Director/AddressCity
	 * Shareholder/AddressCity
	 * Address/City
	 */
	public function getHasCityFieldWithError () {
		
		
		if ($this->getName() != "Director" && $this->getName() != "Shareholder" && $this->getName() != "Address") return false;
		
		foreach ($this->getUsedSectionElements() as $dbComgateData) {
			
			
			if ($dbComgateData->hasSpecifyer("AddressCity") || $dbComgateData->hasSpecifyer("City")) {
				
				if ($dbComgateData->getHasError()) {
					
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * If there is a DbComgateData element in the section that has specifyer = "zip" (part of section after /),
	 * the original field value of that DbComgateData element is returned. Otherwise an empty string
	 * 
	 * @return string
	 */
	public function getZip () {
		foreach ($this->getUsedSectionElements() as $dbComgateData) {
			if ($dbComgateData->hasSpecifyer("Zip")) {
				return $dbComgateData->getOriginal();
			}
		}
	}
	
	public function getCityOriginal () {
		
		foreach ($this->getUsedSectionElements() as $dbComgateData) {
				
			if ($dbComgateData->hasSpecifyer("AddressCity") || $dbComgateData->hasSpecifyer("City")) {
				return $dbComgateData;	
			}
		}
	}
	
	
	/**
	 * If there is a DbComgateData element in the section that has specifyer = "county" (part of section after /),
	 * the original field value of that DbComgateData element is returned. Otherwise an empty string
	 * 
	 * @return string
	 */
	public function getCounty () {
		foreach ($this->getUsedSectionElements() as $dbComgateData) {
			if ($dbComgateData->hasSpecifyer("County")) {
				return $dbComgateData->getOriginal();
			}
		}
	}
	
	/**
	 * If there is a DbComgateData element in the section that has specifyer = "country_code" (part of section after /),
	 * the original field value of that DbComgateData element is returned. Otherwise an empty string
	 * 
	 * @return string
	 */
	public function getCountryOriginal () {
		foreach ($this->getUsedSectionElements() as $dbComgateData) {
			if ($dbComgateData->hasSpecifyer("Country_code") || $dbComgateData->hasSpecifyer("AddressCountry_code")) {
				return $dbComgateData->getOriginal();
			}
		}
	}
	

	
	/**
	 * If there is a DbComgateData element in the section that has specifyer = "country_code" (part of section after /),
	 * the theid field value of that DbComgateData element is returned. Otherwise an empty string
	 * 
	 * @return string
	 */
	public function getCountryOriginalId () {
		foreach ($this->getUsedSectionElements() as $dbComgateData) {
			if ($dbComgateData->hasSpecifyer("Country_code") || $dbComgateData->hasSpecifyer("AddressCountry_code")) {
				return $dbComgateData->getTheid();
			}
		}
		return 0;
	}
	
	public function getCountryCodeDbComgateData () {
		foreach ($this->getUsedSectionElements() as $dbComgateData) {
			if ($dbComgateData->hasSpecifyer("Country_code") || $dbComgateData->hasSpecifyer("AddressCountry_code")) {
				return $dbComgateData;
			}
		}
		return null;
	}
	
	public function getZipDbComgateData () {
		foreach ($this->getUsedSectionElements() as $dbComgateData) {
			if ($dbComgateData->hasSpecifyer("Zip")) {
				return $dbComgateData;
			}
		}
	}
	
	public function getCountyDbComgateData () {
		foreach ($this->getUsedSectionElements() as $dbComgateData) {
			if ($dbComgateData->hasSpecifyer("County")) {
				return $dbComgateData;
			}
		}
	}
	
	public function getCountryCodeSpecifyer () {
		if ($this->getName() == "Director") 		return "AddressCountry_code";
		if ($this->getName() == "Shareholder") 	return "AddressCountry_code";
		if ($this->getName() == "Address")			return "Country_code";
		return "";
	}
	
	public function getDbProg () {
		foreach ($this->getUsedSectionElements() as $dbComgateData) {
			return $dbComgateData->getDbProg();
		}
		return 0;
	}
	
	public function getDbComgateDataBySpecifier ($specifier) {
		foreach ($this->getUsedSectionElements() as $dbComgateData) {
			if ($dbComgateData->hasSpecifyer($specifier)) {
				return $dbComgateData;
			}
		}
		return null;
	}
	
	/**
	 * In case its an address section the method returns a query from its
	 * DbComgateData objects that can be used as a goolge maps link query.
	 * 
	 * @return string
	 */
	public function getGoogleMapsQuery () {
		
		if (!$this->getName() == "Address" && !$this->getName() == "Director"  && !$this->getName() == "Shareholder" ) {
			return "";
		}
		
		if ($this->getName() == "Director" || $this->getName() == "Shareholder") {
			
			$country 	= $this->getDbComgateDataBySpecifier("AddressCountry_code");
			$county 	= $this->getDbComgateDataBySpecifier("AddressCounty");
			$city		= $this->getDbComgateDataBySpecifier("AddressCity");
			$lineOne	= $this->getDbComgateDataBySpecifier("AddressLine1");

		} else {
			$country 	= $this->getCountryCodeDbComgateData();
			$county 	= $this->getCountyDbComgateData();
			$city		= $this->getCityOriginal();
			$lineOne	= $this->getDbComgateDataBySpecifier("Line1");
		}

		
	
		
		$query = "";
		if ($country != null) {
			
			$query .= urlencode($country->getOriginal());
		}
		
		if ($county != null) {
			$query .= "+" . urlencode($county->getOriginal());
		}
		
		if ($city != null) {
			$query .= "+" .urlencode($city->getOriginal());
		}
		
		if ($lineOne != null) {
			$query .= "+" .urlencode($lineOne->getOriginal());
		}
		
		return $query;
	}
	
}

