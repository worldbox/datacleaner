<?php
namespace Worldbox\ComSys\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;
use Doctrine\ORM\Mapping as ORM;
use Worldbox\ComSys\Domain\Repository;

/**
 * A Db comgate data
 *
 * @FLOW3\Entity
 * @ORM\Table(name="DB_COMGATE_DATA")
 */
class DbComgateData {

	/**
	 * @var int
	 * @ORM\Id
	 * @ORM\Column(name="theid", type="integer")	
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $theid;
	

	/**
	 * @var string
	 * @ORM\Column(name="editdate", type="text")	
	 */
	protected $editdate;

	/**
	 * @var int
	 * @ORM\Column(name="user_code", type="integer")	
	 */	
	protected $userCode;


	/**
	 * @var string
	 * @ORM\Column(name="dataelement", type="text", length=255)	
	 */	
	protected $dataelement;

	/**
	 * @var int
	 * @ORM\Column(name="serial", type="integer")	
	 */	
	protected $serial;

	/**
	 * @var int
	 * @ORM\Column(name="verified_status", type="integer")	
	 */	
	protected $verifiedStatus;

	/**
	 * @var string
	 * @ORM\Column(name="original", type="text", length=1000)	
	 */	
	protected $original;

	/**
	 * @var string
	 * @ORM\Column(name="verified_token", type="text", length=1000)	
	 */	
	protected $verifiedToken;

	/**
	 * @var string
	 * @ORM\Column(name="verified_date", type="text")	
	 */	
	protected $verifiedDate;

	/**
	 * @var int
	 * @ORM\Column(name="verified_number", type="integer")	
	 */	
	protected $verifiedNumber;

	/**
	 * @var string
	 * @ORM\Column(name="verified_string", type="text")	
	 */	
	protected $verifiedString;

	/**
	 * @var string
	 * @ORM\Column(name="error", type="text", length=255)	
	 */	
	protected $error;
		
	/**
 	 * The dbComgateComp object this dbComgateData belongs to.
 	 * 
 	 * @var \Worldbox\ComSys\Domain\Model\DbComgate
 	 * @ORM\ManyToOne(inversedBy="dbComgateData")
 	 * @ORM\Column(name="comgate_id", type="text")	
 	 * @ORM\JoinColumn(name="comgate_id", referencedColumnName="theid")
 	 */
	protected $dbComgate;
	
	/**
 	 * The dbComgate object this dbComgateData belongs to.
 	 * 
 	 * @var \Worldbox\ComSys\Domain\Model\DbComgateComp
 	 * @ORM\ManyToOne(inversedBy="dbComgateData")
 	 * @ORM\Column(name="comgate_id_comp", type="text")	
 	 * @ORM\JoinColumn(name="comgate_id_comp", referencedColumnName="theid")
 	 */
	protected $dbComgateComp;

	
	
	public function getTheid () {
		return $this->theid;
	}
	
	public function setTheid ($theid) {
		$this->theid = $theid;
	}
	
	public function getEditdate () {
		return $this->editdate;
	}
	
	public function setEditdate ($editdate) {
		$this->editdate = $editdate;
	}
	
	public function getUserCode  () {
		return $this->userCode;
	}
	
	public function setUserCode ($userCode) {
		$this->userCode = $userCode;
	}
	
	public function getDataelement () {
		return $this->dataelement;
	}
	
	public function setDataelement ($dataelement) {
		$this->dataelement = $dataelement;
	}
	
	public function getSerial () {
		return $this->serial;
	}
	
	public function setSerial ($serial) {
		$this->serial = $serial;
	}
	
	public function getVerifiedStatus () {
		return $this->verifiedStatus;
	}
	
	public function setVerifiedStatus ($verifiedStatus) {
		$this->verifiedStatus = $verifiedStatus;
	}
	
	public function getError () {
		return $this->error;
	}
	
	public function setErrror ($error) {
		$this->error = $error;
	}
	
	public function getHasError () {
		
		if ($this->verifiedStatus == 1) {
			return true;
		}
		return false;
	}
	
	public function getOriginal () {
		return $this->original;
	}
	
	public function setOriginal ($original) {
		$this->original = $original;
	}
	
	public function getVerifiedToken () {
		return $this->verifiedToken;
	}
	
	public function setVerifiedToken ($verifiedToken) {
		$this->verifiedToken = $verifiedToken;
	}
	
	
	/**
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 */
	public function setDbComgate (DbComgate $dbComgate) {
		$this->dbComgate = $dbComgate;
	}
	
	/**
	 * 
	 * @return \Worldbox\ComSys\Domain\Model\DbComgate
	 */
	public function getDbComgate () {
		return $this->dbComgate;
	}
	
	/**
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp
	 */
	public function setDbComgateComp (DbComgateComp $dbComgateComp) {
		$this->dbComgateComp = $dbComgateComp;
	}
	
	/**
	 * 
	 * @return \Worldbox\ComSys\Domain\Model\DbComgateComp
	 */
	public function getDbComgateComp () {
		return $this->dbComgateComp;
	}
	
	/**
	 * Returns the part before the / of the dataelement field.
	 * This is called the section.
	 * 
	 * @return string
	 */
	public function getSection () {

		preg_match("/^(.+)\//", $this->dataelement, $match);
		if (isset($match[1])) {
			return $match[1];
		}
		return "";
	}
	
	/**
	 * Returns the part of the section after /
	 * 
	 */
	public function getSpecifyer () {
		preg_match("/\/(.+)$/", $this->dataelement, $match);
		if (isset($match[1])) {
			return $match[1];
		}
		return "";
	}
	
	public function hasSpecifyer ($specifyer) {
		if (strtoupper($this->getSpecifyer()) == strtoupper($specifyer)) {
			return true;
		}
		return false;
	}
	
	/**
	 * Returns $this->dataelement
	 * @return string
	 */
	public function getSectionAndSpecifyer () {
		return $this->dataelement;
	}
	
	/**
	 * Returns true if thie DbComgateData has the given $datalement.
	 * 
	 * @param string $sectionAndSpecifier
	 * @return boolean
	 */
	public function hasSectionAndSpecifyer ($sectionAndSpecifier) {
		if ($sectionAndSpecifier == $this->dataelement) {
			return true;
		}
		return false;
	}
	
	
	/**
	 * Returns true if the DbComgateData element is editable. This is the 
	 * case if it has an error or another DbComgateData element with
	 * the same serial has an error.
	 * 
	 * @return boolean
	 */
	public function getIsEditable () {
		
		
		$sameSerial = $this->dbComgate->getDbComgateDataWithSerial($this->getSerial());
		foreach ($sameSerial as $dbComgateData) {
			if ($dbComgateData->getHasError() && $dbComgateData->getSection() == $this->getSection()) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns true if this dbComgateData element has the given serial $serial.
	 * 
	 * @param int $serial
	 * @return boolean
	 */
	public function hasSerial ($serial) {
		if ((int)$serial == (int)$this->getSerial()) {
			return true;			
		}
		return false;
	}
	
	
	/**
	 * Returns true if this dbComgateData element represents information about a city.
	 * @return boolean
	 */
	public function getIsCityFiled ( ) {
		if ($this->getSection() ==  "Director" || $this->getSection() ==  "Shareholder"  || $this->getSection() ==  "Address" ) {
			if ($this->getSpecifyer() == "City" || $this->getSpecifyer() == "AddressCity") {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns the section object that contains this dbComgateData object.
	 * 
	 */
	public function getSectionOfDbComgateData () {
		$sections				= $this->getDbComgate()->getDbComgateDataGroupedBySection();
		foreach ($sections as $section) {
			if ($section->getName() == $this->getSection() && $section->getSerial() == $this->getSerial()) {
				return $section;
			}
		}
		return null;
	}
	

}
?>