<?php
namespace Worldbox\ComSys\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */

use Worldbox\ComSys\Log\ComSysLogger;

use TYPO3\FLOW3\Annotations as FLOW3;
use Doctrine\ORM\Mapping as ORM;
use Worldbox\ComSys\Domain\Repository;
use TYPO3\FLOW3\Core\Bootstrap;

/**
 * A Db comgate
 *
 * @FLOW3\Entity
 * @ORM\Table(name="DB_COMGATE_COMP")
 */
class DbComgateComp {


	/**
	 * @var int
	 * @ORM\Id
	 * @ORM\Column(name="theid", type="integer")	
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $theid;
	
	/**
 	 * The dbComgate object this dbComgateComp belongs to.
 	 * 
 	 * @var \Worldbox\ComSys\Domain\Model\DbComgate
 	 * @ORM\ManyToOne(inversedBy="dbComgateData")
 	 * @ORM\Column(name="comgate_id", type="text")	
 	 * @ORM\JoinColumn(name="comgate_id", referencedColumnName="theid")
 	 */
	protected $dbComgate;
	
	/**
	 * @var string
	 * @ORM\Column(name="source_id", type="text")	
	 */	
	protected $sourceId;
	
	/**
	 * @var string
	 * @ORM\Column(name="win", type="text")	
	 */	
	protected $win;
	
	/**
	 * @var string
	 * @ORM\Column(name="start_date", type="text")	
	 */
	protected $startDate;
	
	/**
	 * @var string
	 * @ORM\Column(name="verified_date", type="text")	
	 */
	protected $verifiedDate;
	
		/**
	 * @var string
	 * @ORM\Column(name="integrated_date", type="text")	
	 */
	protected $integratedDate;
	
	/**
	 * @var int
	 * @ORM\Column(name="match_status", type="integer")	
	 */	
	protected $matchStatus;
	
	/**
	 * @var int
	 * @ORM\Column(name="inactive", type="integer")	
	 */	
	protected $inactive;
	
	/**
	 * @var int
	 * @ORM\Column(name="verified_status", type="integer")	
	 */	
	protected $verifiedStatus;
	
	/**
	 * @var string
	 * @ORM\Column(name="role_type", type="text")	
	 */
	protected $roleType;
	
	/**
	 * The dbComgateData objects that are assigned to this dbComgateComp object.
	 *
	 * @var \Doctrine\Common\Collections\Collection<Worldbox\ComSys\Domain\Model\DbComgateData>
	 * @ORM\OneToMany(mappedBy="dbComgateComp")
	 * @ORM\OrderBy({"dataelement" = "DESC"})
	 */
	protected $dbComgateData;
	
	
	/**
	 * 
	 * @return int
	 */
	public function getTheid () {
		return $this->theid;
	}
	
	/**
	 * 
	 * @param int $theid
	 */
	public function setTheid ($theid) {
		$this->theid = $theid;
	}
	
	/**
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 */
	public function setDbComgate (DbComgate $dbComgate) {
		$this->dbComgate = $dbComgate;
	}
	
	/**
	 * 
	 * @return \Worldbox\ComSys\Domain\Model\DbComgate
	 */
	public function getDbComgate () {
		return $this->dbComgate;
	}
	
	/**
	 * 
	 * @return \Doctrine\Common\Collections\Collection<Worldbox\ComSys\Domain\Model\DbComgateData>
	 */
	public function getDbComgateData () {
		return $this->dbComgateData;
	}
	
	public function getSourceId () {
		return $this->sourceId;
	}
	
	
	public function setSourceId ($sourceId) {
		$this->sourceId = $sourceId;
	}
	
	public function getWin () {
		return $this->win;
	}
	
	
	public function setWin ($win) {
		$this->win = $win;
	}
	
	
	public function getStartDate () {
		return $this->startDate;
	}
	
	
	public function setStartDate ($startDate) {
		$this->startDate = $startDate;
	}
	
	
	public function getVrifiedDate () {
		return $this->verifiedDate;
	}
	
	
	public function setVerifiedDate ($verifiedDate) {
		$this->verifiedDate = $verifiedDate;
	}
	
	public function getIntegratedDate () {
		return $this->integratedDate;
	}
	
	
	public function setIntegratedDate ($integratedDate) {
		$this->integratedDate = $integratedDate;
	}
	
	
	public function getMatchStatus () {
		return $this->matchStatus;
	}
	
	
	public function setMatchStatu ($matchStatus) {
		$this->matchStatus = $matchStatus;
	}
	
	
	public function getInactive () {
		return $this->inactive;
	}
	
	
	public function setInactive ($inactive) {
		$this->inactive = $inactive;
	}
	
	
	public function getVerifiedStatus () {
		return $this->verifiedStatus;
	}
	
	
	public function setVerifiedStatus ($verifiedStatus) {
		$this->verifiedStatus = $verifiedStatus;
	}
	
	public function getRoleType () {
		return $this->roleType;
	}
	
	public function setRoleType ($roleType) {
		$this->roleType = $roleType;
	}
}
?>