<?php
namespace Worldbox\ComSys\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */

use Worldbox\ComSys\Log\ComSysLogger;

use TYPO3\FLOW3\Annotations as FLOW3;
use Doctrine\ORM\Mapping as ORM;
use Worldbox\ComSys\Domain\Repository;
use TYPO3\FLOW3\Core\Bootstrap;
use TYPO3\FLOW3\Annotations\Transient;

/**
 * A Db comgate
 *
 * @FLOW3\Entity
 * @ORM\Table(name="DB_COMGATE")
 */
class DbComgate {


	/**
	 * @var int
	 * @ORM\Id
	 * @ORM\Column(name="theid", type="integer")	
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $theid;
	
	/**
	 * @var string
	 * @ORM\Column(name="editdate", type="text")	
	 */
	protected $editdate;
	
	/**
	 * @var string
	 * @ORM\Column(name="user_code", type="text")	
	 */
	protected $userCode;
		
	/**
	 * @var string
	 * @ORM\Column(name="order_date", type="text")	
	 */
	protected $orderDate;
	
	/**
	 * @var string
	 * @ORM\Column(name="spider_date", type="text")	
	 */
	protected $spiderDate;
	
	/**
	 * @var string
	 * @ORM\Column(name="verified_date", type="text")	
	 */
	protected $verifiedDate;
	
	/**
	 * @var string
	 * @ORM\Column(name="integrated_date", type="text")	
	 */
	protected $integratedDate;
	
	/**
	 * @var string
	 * @ORM\Column(name="url", type="text")	
	 */
	protected $url;
	
	/**
	 * @var string
	 * @ORM\Column(name="win", type="text")	
	 */
	protected $win;
	
	/**
	 * @var int
	 * @ORM\Column(name="verified_status", type="integer")	
	 */
	protected $verifiedStatus;
	
	/**
	 * @var int
	 * @ORM\Column(name="inactive", type="integer")	
	 */
	protected $inactive;
	
	
	/**
	 * The dbComgateData objects that are assigned to this dbComgate object.
	 *
	 * @var \Doctrine\Common\Collections\Collection<Worldbox\ComSys\Domain\Model\DbComgateData>
	 * @ORM\OneToMany(mappedBy="dbComgate")
	 * @ORM\OrderBy({"dataelement" = "DESC"})
	 * 
	 */
	protected $dbComgateData;
	
	/**
	 * The dbComgateComp objects that are assigned to this dbComgate object.
	 *
	 * @var \Doctrine\Common\Collections\Collection<Worldbox\ComSys\Domain\Model\DbComgateComp>
	 * @ORM\OneToMany(mappedBy="dbComgate")
	 * @ORM\OrderBy({"theid" = "DESC"})
	 * 
	 */
	protected $dbComgateComp;
	
	
	/**
	 * @var int
	 * @ORM\Column(name="prog_code", type="integer")	
	 */
	protected $progCode;
	
	/**
 	 * The dbProg object this dbComgate belongs to.
 	 * 
 	 * @var \Worldbox\ComSys\Domain\Model\DbProg
 	 * @ORM\ManyToOne(inversedBy="dbComgate")
 	 * @ORM\Column(name="prog_code", type="int")	
 	 * @ORM\JoinColumn(name="prog_code", referencedColumnName="prog_code")
 	 */
	protected $dbProg;
	
	/**
     * @var string
     * @Transient
	 */
	protected $dbIdentName;
	
	/**
     * @var string
     * @Transient
	 */
	protected $dbIdentCountryName;
	
	/**
     * @var string
     * @Transient
	 */
	protected $dbIdentCountryCode;
	
	
	public function getDbIdentName (){
		return $this->dbIdentName;
	}
	
	public function setDbIdentName ($dbIdentName) {
		$this->dbIdentName = $dbIdentName; 
	}
	
	public function getDbIdentCountryName (){
		return $this->dbIdentName;
	}
	
	public function setDbIdentCountryName ($dbIdentCountryName) {
		$this->dbIdentCountryName = $dbIdentCountryName; 
	}
	
	public function getDbIdentCountryCode (){
		return $this->dbIdentCountryCode;
	}
	
	public function setDbIdentCountryCode  ($dbIdentCountryCode ) {
		$this->dbIdentCountryCode = $dbIdentCountryCode; 
	}
	
	public function setInactive ($inactive) {
		$this->inactive = $inactive;
	}


	
	/**
	 * 
	 * @return int
	 */
	public function getTheid () {
		return $this->theid;
	}
	
	/**
	 * 
	 * @param int $theid
	 */
	public function setTheid ($theid) {
		$this->theid = $theid;
	}
	
	
	public function getDbProg () {
		return $this->dbProg;
	}
	
	
	
	/**
	 * 
	 * @return string
	 */
	public function getEditdate () {
		return $this->editdate;
	}
	
	/**
	 * 
	 * @param string $editdate
	 */
	public function setEditdate ($editdate) {
		$this->editdate = $editdate;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function getUrl () {
		return $this->url;
	}
	
	/**
	 * 
	 * @param string $editdate
	 */
	public function setUrl ($url) {
		$this->url = $url;
	}
	
	
	public function getVerifiedStatus () {
		return $this->verifiedStatus;
	}
	
	
	public function setVerifiedStatus ($verifiedStatus) {
		$this->verifiedStatus = $verifiedStatus;
	}
	
	public function getVerifiedDate () {
		return $this->verifiedDate;
	}
	
	public function setVerifiedDate ($verifiedDate) {
		$this->verifiedDate = $verifiedDate;
	}
	
	public function getProgCode () {
		return $this->progCode;
	}
	
	public function setProgCode ($progCode) {
		$this->progCode = $progCode;
	}
	
	/**
	 * Sets the verifiedStatus to 0. This indicates, that
	 * the dbComgateData has no more errors. The method does not
	 * check, whether there are dbComgateData objects assigned to
	 * it that have errors.
	 * 
	 */
	public function markAsNoMoreErrors () {
		$this->setVerifiedStatus(0);
	}
	
	
	/**
	 * Adds a post to this blog
	*
	 * @param \TYPO3\Blog\Domain\Model\Post $post
	 * @return void
	  */
	public function addDbComgateData(\Worldbox\ComSys\Domain\Model\DbComgateData $dbComgateData) {
		$dbComgateData->setDbComgate($this);
		$this->dbComgateData->add($dbComgateData);
	}

	/**
	 * Removes a post from this blog
	 *
	 * @param \TYPO3\Blog\Domain\Model\Post $post
	 * @return void
	 */
	public function removeDbComgateData(\Worldbox\ComSys\Domain\Model\DbComgateData $dbComgateData) {
		$this->dbComgateData->removeElement($dbComgateData);
	}
	
	/**
	 * 
	 * @return \Doctrine\Common\Collections\Collection<Worldbox\ComSys\Domain\Model\DbComgateData>
	 */
	public function getDbComgateData () {
		return $this->dbComgateData;
	}
	
	
	/**
	 * 
	 * @return \Doctrine\Common\Collections\Collection<Worldbox\ComSys\Domain\Model\DbComgateComp>
	 */
	public function getDbComgateComp () {
		return $this->dbComgateComp;
	}
	
	
	
	
	/**
	 * Returns a list of all sections tha dbComgateData element of this
	 * dbComgate object contain. The section is the first part of the
	 * string in the dbComgateData table column dataelement (e.g. 
	 * Address/Country_code)
	 * 
	 * @return array
	 */
	public function getSections () {
		
		$availableSections = array();
		foreach ($this->dbComgateData as $dbComgateData) {
			$currentSection = $dbComgateData->getSection();
			$alreadyFound	= false;
			foreach ($availableSections as $availableSection) {
				if ($currentSection == $availableSection) {
					$alreadyFound = true;
				}
			}
			if ($alreadyFound == false) {
				$availableSections[] = $currentSection;
			}
		}

		return $availableSections;
	}
	
	/**
	 * Returns all dbComgateDate elements of this dbComgate that
	 * belogn to the section given by $section.
	 * 
	 * @param string $section
	 * @return array
	 */
	public function getDbComgateDataBySection ($section, $serial = false) {
		
		$dbComgateDataFiltered = array();
		
		foreach ($this->dbComgateData as $dbComgateData) {
			if ($dbComgateData->getSection() == $section) {
				if ($serial == false) {
					$dbComgateDataFiltered[] = $dbComgateData;
				} else {
					if ($dbComgateData->getSerial() == $serial) {
						$dbComgateDataFiltered[] = $dbComgateData;
					}
				}
				
			}
		}
		
	
		return $dbComgateDataFiltered;
	}
	
	/**
	 * Returns true if this DbComgate has a DbComgateData with 
	 * the given section $section.
	 * 
	 * @param string $section
	 */
	public function sectionExists ($section) {
		foreach ($this->dbComgateData as $dbComgateData) {
			if ($dbComgateData->hasSectionAndSpecifyer($section)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns all serials that are used within the given section.
	 * 
	 * @return array
	 */
	public function getUsedSerialsOfSection ($section) {
		
		$dbComgateDataOfSection 	= $this->getDbComgateDataBySection($section);
		$usedSerials 				= array();
		foreach ($dbComgateDataOfSection as $dbComgateData) {
			if (!in_array($dbComgateData->getSerial(), $usedSerials)) {
				$usedSerials[] = $dbComgateData->getSerial();
				
			}
		}
		
		return $usedSerials;
	}
	
	/**
	 * Returns all dbComgateData objects of this dbComgate grouped
	 * by section. The result is a two dimensional array with
	 * the keys of the toplevel array as sections.
	 * 
	 * @return array
	 */
	public function getDbComgateDataGroupedBySection () {
		
		$sections = array();
		
		foreach ($this->getSections() as $sectionName) {
			
			$usedSerials															= $this->getUsedSerialsOfSection($sectionName);
			$multipleSectionsWithSameName = false;
			if (count($usedSerials) > 1) {
				
				$multipleSectionsWithSameName = true;
			} 
			
			
			foreach ($usedSerials as $usedSerial) {
				$section = new DbComgateDataSection();
				$section->setName($sectionName);
				$section->setSectionWithSameNameExists($multipleSectionsWithSameName);
				$section->setSerial($usedSerial);
				$section->setNotUsedSectionElements($this->getNotUsedSections($sectionName, $usedSerial));
				$section->setUsedSectionElements($this->getDbComgateDataBySection($sectionName, $usedSerial));
				$sections[] = $section;
			}
		}
		
		return $sections;
		
		$dbComgateDataGrouped = array();
		foreach ($this->getSections() as $section) {
			
			$usedSerials															= $this->getUsedSerialsOfSection($section);
			foreach ($usedSerials as $usedSerial) {
				$dbComgateDataGrouped[$section][$usedSerial]['data'] 				= $this->getDbComgateDataBySection($section, $usedSerial);
				$dbComgateDataGrouped[$section][$usedSerial]['unUsedSections'] 		= $this->getNotUsedSections($section, $usedSerial);
			}
			
		}
		
		$section = new DbComgateDataSection();
		return $dbComgateDataGrouped;		
	}
	
	
	/**
	 * Returns true if there is a dbComgateData element that has
	 * an error. Otherwise false.
	 * 
	 * @return boolen
	 */
	public function hasError () {
		if ($this->getNumberOfErrors () > 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * Returns the number of dbComgateData elements that have
	 * an error -> verifiedStatus = 1.
	 * 
	 * @return int
	 */
	public function getNumberOfErrors () {
		$numberOfErrors = 0;
		foreach ($this->dbComgateData as $dbComgateData) {
			if ($dbComgateData->getHasError()) {
				$numberOfErrors++;
			}
		}
		return $numberOfErrors;
	}
	
	/**
	 * Returns all section items of the section given by $section that are not used currently. 
	 * Thus there is no DbComgateData element assigned to this DbComgate with the section.
	 *
	 * This is done by a raw sql query to the DB_XPATH_MAP table
	 * 
	 * @return array
	 */
	public function getNotUsedSections ($section, $serial = false) {
		
		$conn 				= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection(); 
		$rawQuery 			= "SELECT DISTINCT section, field FROM LINK_SECTION_FIELDS WHERE section= '" . $section . "' ORDER BY field";
		$result 			= $conn->executeQuery($rawQuery)->fetchAll();
		
		
		$unUsedSections		= array();
		$usedSections		= $this->getDbComgateDataBySection($section, $serial);
		foreach ($result as $row) {
			$isUsed = false;
			
			foreach ($usedSections as $usedSection) {
				
				if ($row['section'] . "/" . $row['field'] == $usedSection->getDataelement()) {
					$isUsed = true;
				}
			}
			if ($isUsed == false) {
				$unUsedSections[] = $row['section'] . "/" . $row['field'];	
			}
		}
		return $unUsedSections;
	
	}
	
	/**
	 * Returns all DbComgateData elements with the given serial $serial.
	 * 
	 * @param int $serial
	 * @return array
	 */
	public function getDbComgateDataWithSerial ($serial) {
		$sameSerial = array();
		foreach ($this->dbComgateData as $dbComgateDataElement) {
			if ($dbComgateDataElement->hasSerial($serial)) {
				$sameSerial[] = $dbComgateDataElement;
			}
		}
		return $sameSerial;
	}
	
	/**
	 * Returns the DbComgateData element of this DbComgate data with dataelement
	 * having specifyer equal to the parameter $dataElement. If not existent it returns false.
	 * 
	 * !! If you forgot what a specifyer is:
	 * DbComgateDate.dataelement has the form "Section/Specifyer" e.g Name/Name (second part after / is specifier)
	 * 
	 * @param string $dataElement
	 */
	public function getDbComgateDateWithSpecifyer ($specifyer) {
		
		$dbComgateDataWithSpecifier = array();
		foreach ($this->getDbComgateData() as $dbComgateData) {
			if ($dbComgateData->hasSpecifyer($specifyer)) {
				$dbComgateDataWithSpecifier[] =  $dbComgateData;
			}
		}
		return $dbComgateDataWithSpecifier;
	}
	
	/**
	 * Returns the dbComgateData element with the given section and specifier or false
	 * if not existent.
	 * 
	 * @param string $section
	 * @param string $specifier
	 */
	public function getDbComgateDataWithSectionAndSpecifier ($section, $specifier) {
		
		$dbComgateDataElements = $this->getDbComgateDataBySection($section);
		if (is_array($dbComgateDataElements)) {
			foreach ($dbComgateDataElements as $dbComgateData ) {
				if ($dbComgateData->hasSpecifyer($specifier)) return $dbComgateData;
			}
		}
		return false;
	}
	
	
	public function getWin () {
		return $this->win;
	}
	
	public function setWin ($win) {
		$this->win = $win;
	}
}
?>