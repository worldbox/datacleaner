<?php
namespace Worldbox\ComSys\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;
use Doctrine\ORM\Mapping as ORM;
use Worldbox\ComSys\Domain\Repository;
use TYPO3\FLOW3\Core\Bootstrap;

/**
 * A Db prog
 *
 * @FLOW3\Entity
 * @ORM\Table(name="DB_PROG")
 */
class DbProg {

	
	/**
	 * @var int
	 * @ORM\Id
	 * @ORM\Column(name="theid", type="integer")	
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $theid;
	
	/**
	 * @var string
	 * @ORM\Column(name="restrictions", type="text")	
	 */
	protected $restrictions;
	
	/**
	 * @var string
	 * @ORM\Column(name="description", type="text")	
	 */
	protected $description;
	
	/**
	 * @var string
	 * @ORM\Column(name="end_date", type="text")	
	 */
	protected $endDate;
	
	/**
	 * @var string
	 * @ORM\Column(name="start_date", type="text")	
	 */
	protected $startDate;
	
	/**
	 * @var string
	 * @ORM\Column(name="prog_country_code", type="text")	
	 */
	protected $progCountryCode;
	
	/**
	 * @var string
	 * @ORM\Column(name="progname", type="text")	
	 */
	protected $progname;	
	
	/**
	 * @var string
	 * @ORM\Column(name="progqual", type="text")	
	 */
	protected $progqual;
	
	/**
	 * @var string
	 * @ORM\Column(name="progtype", type="text")	
	 */
	protected $progtype;
	
	/**
	 * @var string
	 * @ORM\Column(name="user_code", type="text")	
	 */
	protected $userCode;	
	
	/**
	 * @var string
	 * @ORM\Column(name="editdate", type="text")	
	 */
	protected $editdate;
	
	/**
	 * @var string
	 * @ORM\Column(name="prog_code", type="text")	
	 */
	protected $progCode;	
	
	/**
	 * The dbComgate objects that are assigned to this dbProg object.
	 *
	 * @var \Doctrine\Common\Collections\Collection<Worldbox\ComSys\Domain\Model\DbComgate>
	 * @ORM\OneToMany(mappedBy="dbProg", fetch="LAZY")
	 */
	protected $dbComgate;
	
	
	public function getTheid () {
		
		return $this->theid;
	}
	
	public function getRestrictions () {
		return $this->restrictions;
	}
	
	public function getDescription () {
		return $this->description;
	}

	public function getEndDate () {
		return $this->endDate;
	}
	
	public function getStartDate () {
		return $this->startDate;
	}
	
	public function getProgCountryCode () {
		return $this->progCountryCode;
	}
	
	public function getProgname () {
		return $this->progname;
	}
	
	public function setProgname ($progname) {
		$this->progname = $progname;
	}
	
	public function getProgqual () {
		return $this->progqual;
	}
	
	public function getProgtype () {
		return $this->progtype;
	}	
	
	public function getUserCode () {
		return $this->userCode;
	}
	
	public function getEditdate () {
		return $this->editdate;
	}
	
	public function getProgCode () {
		return $this->progCode;
	}
	
	/**
	 * @return \Doctrine\Common\Collections\Collection<Worldbox\ComSys\Domain\Model\DbComgate>
	 */
	public function getDbComgate () {
		return $this->dbComgate;
	}
	
	
	/**
	 * Returns the number of DbComgate objects of this DbProg with
	 * verifiedStatus = 0. Thus thoes with errors.
	 * 
	 * @return int
	 */
	public function getNumberOfDbComgateErrors () {
		
		$conn 				= Bootstrap::$staticObjectManager->get("Doctrine\Common\Persistence\ObjectManager")->getConnection(); 
		$rawQuery 			= "SELECT COUNT(*) FROM DB_COMGATE WHERE prog_code = " . $this->getProgCode() . " AND verified_status = 1";
		$result 			= $conn->executeQuery($rawQuery)->fetchAll();
		
		 
		foreach ($result as $row) {

			return (int) $row[0];
		}
	
	}
}
?>