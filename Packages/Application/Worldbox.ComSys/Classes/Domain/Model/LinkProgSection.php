<?php
namespace Worldbox\ComSys\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */

use Worldbox\ComSys\Log\ComSysLogger;

use TYPO3\FLOW3\Annotations as FLOW3;
use Doctrine\ORM\Mapping as ORM;
use Worldbox\ComSys\Domain\Repository;
use TYPO3\FLOW3\Core\Bootstrap;

/**
 * A Db comgate
 *
 * @FLOW3\Entity
 * @ORM\Table(name="LINK_PROG_SECTION")
 */
class LinkProgSection {


	/**
	 * @var int
	 * @ORM\Id
	 * @ORM\Column(name="theid", type="integer")	
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $theid;
	
	
	/**
	 * @var int
	 * @ORM\Column(name="qualitylevel", type="integer")	
	 */
	protected $qualitylevel;

	
	/**
	 * @var int
	 * @ORM\Column(name="safeperiode", type="integer")	
	 */
	protected $safeperiode;

	/**
	 * @var string
	 * @ORM\Column(name="section_code", type="string")	
	 */
	protected $sectionCode;
	
	/**
	 * @var string
	 * @ORM\Column(name="integrationcondition", type="string")	
	 */
	protected $integrationcondition;

	
	/**
 	 * The dbProg object this dbComgate belongs to.
 	 * 
 	 * @var \Worldbox\ComSys\Domain\Model\DbProg
 	 * @ORM\ManyToOne(inversedBy="dbComgate")
 	 * @ORM\Column(name="prog_code", type="int")	
 	 * @ORM\JoinColumn(name="prog_code", referencedColumnName="prog_code")
 	 */
	protected $dbProg;
	
	
	/**
	 * 
	 * @return int
	 */
	public function getTheid () {
		return $this->theid;
	}
	
	/**
	 * 
	 * @param int $theid
	 */
	public function setTheid ($theid) {
		$this->theid = $theid;
	}
	
	
	public function getQualitylevel() {
		return $this->qualitylevel;
	}
	
	
	public function setQualitylevel($qualitylevel) {
		$this->qualitylevel = $qualitylevel;
	}
	
	
	public function getSafeperiode() {
		return $this->safeperiode;
	}
	
	
	public function setSafeperiode($safeperiode) {
		$this->saveperiode = $safeperiode;
	}
	
	
	public function getSectionCode() {
		return $this->sectionCode;
	}
	
	
	public function setSectionCode($sectionCode) {
		$this->sectionCode = $sectionCode;
	}
	
	
	public function getIntegrationcondition() {
		return $this->integrationcondition;
	}
	
	
	public function setIntegrationcondition($integrationcondition) {
		$this->integrationcondition = $integrationcondition;
	}
	
	public function getDbProg () {
		return $this->dbProg;
	}


}
?>