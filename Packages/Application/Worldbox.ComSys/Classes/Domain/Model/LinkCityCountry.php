<?php

namespace Worldbox\ComSys\Domain\Model;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */

use Worldbox\ComSys\Log\ComSysLogger;

use TYPO3\FLOW3\Annotations as FLOW3;
use Doctrine\ORM\Mapping as ORM;
use Worldbox\ComSys\Domain\Repository;
use TYPO3\FLOW3\Core\Bootstrap;

/**
 * A Db comgate
 *
 * @FLOW3\Entity
 * @ORM\Table(name="LINK_CITY_COUNTRY")
 */
class LinkCityCountry {


	/**
	 * @var int
	 * @ORM\Id
	 * @ORM\Column(name="theid", type="integer")	
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $theid;
	
	/**
	 * @var int
	 * @ORM\Column(name="source_unique", type="integer")	
	 */
	protected $sourceUnique;
				
	/**
	 * @var string
	 * @ORM\Column(name="attribute_1", type="text")	
	 */
	protected $attribute1 = "";
	
	/**
	 * @var string
	 * @ORM\Column(name="attribute_2", type="text")	
	 */
	protected $attribute2 = "";
	
	/**
	 * @var string
	 * @ORM\Column(name="attribute_3", type="text")	
	 */
	protected $attribute3 = "";
		
	/**
	 * @var string
	 * @ORM\Column(name="county", type="text")	
	 */
	protected $county;
	
	/**
	 * @var string
	 * @ORM\Column(name="source_date", type="text")	
	 */
	protected $sourceDate;
		
	/**
	 * @var string
	 * @ORM\Column(name="source", type="text")	
	 */
	protected $source;
	
	/**
	 * @var string
	 * @ORM\Column(name="zip", type="text")	
	 */
	protected $zip;
	
	/**
	 * @var string
	 * @ORM\Column(name="city", type="text")	
	 */
	protected $city;
		
	/**
	 * @var string
	 * @ORM\Column(name="source_city", type="text")	
	 */
	protected $sourceCity;
		
	/**
	 * @var string
	 * @ORM\Column(name="country_code", type="text")	
	 */
	protected $countryCode;
	
	/**
	 * @var string
	 * @ORM\Column(name="source_city_uc", type="text")	
	 */
	protected $sourceCityUc;

	

	/**
	 * 
	 * @return int
	 */
	public function getTheid () {
		return $this->theid;
	}
	
	/**
	 * 
	 * @param int $theid
	 */
	public function setTheid ($theid) {
		$this->theid = $theid;
	}
	
	/**
	 * 
	 * @return int
	 */
	public function setSourceUnique ($sourceUnique) {
		$this->sourceUnique = $sourceUnique;
	}
	
	/**
	 * 
	 * @param int $sourceUnique
	 */
	public function getSourceUnique () {
		
		return $this->sourceUnique;
	}
	
	/**
	 * 
	 * @return int
	 */
	public function setAttribute1 ($attribute1) {
		$this->attribute1 = $attribute1;
	}
	
	/**
	 * 
	 * @param int $attribute1
	 */
	public function getAttribute1 () {
		
		return $this->attribute1;
	}
	
	/**
	 * 
	 * @return int
	 */
	public function setAttribute2 ($attribute2) {
		$this->attribute2 = $attribute2;
	}
	
	/**
	 * 
	 * @param int $attribute2
	 */
	public function getAttribute2 () {
		
		return $this->attribute2;
	}
	
	/**
	 * 
	 * @return int
	 */
	public function setAttribute3 ($attribute3) {
		$this->attribute3 = $attribute3;
		
	}
	
	/**
	 * 
	 * @param int $attribute3
	 */
	public function getAttribute3 () {
		return $this->attribute3;
	}
	
	/**
	 * 
	 * @return int
	 */
	public function setCounty ($county) {
		$this->county = $county;
	}
	
	/**
	 * 
	 * @param int $country
	 */
	public function getCounty() {
		return $this->county;
	}
	
	/**
	 * 
	 * @return int
	 */
	public function setSourceDate ($sourceDate) {
		$this->sourceDate = $sourceDate;
	}
	
	/**
	 * 
	 * @param int $sourceDate
	 */
	public function getSourceDate() {
		
		return $this->sourceDate;
	}
	
	/**
	 * 
	 * @return int
	 */
	public function setSource ($source) {
		$this->source = $source;
	}
	
	/**
	 * 
	 * @param int $source
	 */
	public function getSource() {
		
		return $this->source;
	}
	
	/**
	 * 
	 * @return int
	 */
	public function setZip ($zip) {
		
		$this->zip = $zip;
	}
	
	/**
	 * 
	 * @param int $zip
	 */
	public function getZip () {
		return $this->zip;
	}
	
	/**
	 * 
	 * @return string
	 */
	public function setCity ($city) {
		$this->city = $city;
		
	}
	
	/**
	 * 
	 * @param int city
	 */
	public function getCity () {
		return $this->city;
	}
	
	/**
	 * 
	 */
	public function getSourceCityUc () {
		return $this->sourceCityUc;
	}
	
	
	/**
	 * 
	 * @return int
	 */
	public function setSourceCityUc ($sourceCityUc) {
		$this->sourceCityUc = $sourceCityUc;
		
	}
	
	/**
	 * 
	 * @return int
	 */
	public function setSourceCity ($sourceCity) {
		$this->sourceCity = $sourceCity;
	}
	
	/**
	 * 
	 * @param int $sourceCity
	 */
	public function getSourceCity () {
		return $this->sourceCity;
	}
	
	/**
	 * 
	 * @return int
	 */
	public function setCountryCode ($countryCode) {
		$this->countryCode = $countryCode;
	}
	
	/**
	 * 
	 * @param int $countryCode
	 */
	public function getCountryCode () {
		
		return $this->countryCode;
	}
	
}
?>
