<?php
namespace Worldbox\ComSys\Controller;



use Worldbox\ComSys\Log\ComSysLogger;

use TYPO3\FLOW3\Persistence\Doctrine\PersistenceManager;

use Worldbox\ComSys\Domain\Model\DbComgate;

use Worldbox\ComSys\Domain\Model\DbComgateData;

use Worldbox\ComSys\Domain\Model\LinkCityCountry;

use TYPO3\FLOW3\Annotations as FLOW3;

use Doctrine\Common\DateTime\DateTime;

use TYPO3\FLOW3\Log\Logger;

use TYPO3\FLOW3\Annotations\IgnoreValidation;


/**
 * WIN Matching Companies
 *
 * @FLOW3\Scope("singleton")
 */
class WinMatchCompController extends \TYPO3\FLOW3\MVC\Controller\ActionController {

	
	/**
	 * @FLOW3\Inject
	 * @var Worldbox\ComSys\Log\ComSysLogger
	 */
	protected $comSysLogger;
	
	/**
	 * @FLOW3\Inject
	 * @var Worldbox\ComSys\Domain\Repository\DbProgRepository
	 */
	protected $dbProgRepository;
	
	/**
	 * @FLOW3\Inject
	 * @var Worldbox\ComSys\Domain\Repository\DbComgateRepository
	 */
	protected $dbComgateRepository;
	
	
	
	/**
	 * @FLOW3\Inject
	 * @var Worldbox\ComSys\Domain\Repository\DbComgateDataRepository
	 */
	protected $dbComgateDataRepository;
	
	/**
	 * @FLOW3\Inject
	 * @var Worldbox\ComSys\Domain\Repository\LinkCityCountryRepository
	 */
	protected $linkCityCountryRepository;
	
	/**
	 * @FLOW3\Inject
	 * @var Worldbox\ComSys\Domain\Repository\LinkProgSectionRepository
	 */
	protected $linkProgSectionRepository;
	
	/**
	 * @FLOW3\Inject
	 * @var Worldbox\ComSys\Domain\Repository\DbComgateCompRepository
	 */
	protected $dbComgateCompRepository;
	
	
	
	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\FLOW3\Log\Logger
	 */
	protected $logger;
	
	
	public static $CIM_SYS_URL		= "http://cimsys.stage.worldbox.ch/view_partyIdent.cfm?ID=";
	
	
	/**
	 * Webservice URL:
	 * http://secure.newdev.worldbox.ch/ws_comgate/dataGateway.cfc?wsdl
	 * http://secure.stage.worldbox.ch/secure/wwwroot/ws_comgate/dataGateway.cfc?wsdl		Given by Anita I think
	 * http://secure.newProd.worldbox.ch/ws_comgate/dataGateway.cfc?wsdl 
	 * @var unknown_type
	 */
	public static $WEB_SERVICE_WSDL = "http://secure.newdev.worldbox.ch/ws_comgate/dataGateway.cfc?wsdl"; 
	
	const WEB_SERVICE_USER 	= "gan";
	
	const WEB_SERVICE_PASS = "anita22!";
	
	const NUMBER_OF_MAIN_COMPANIES_PER_CALL = 10;
	
	protected $totalWebServiceTime = 0;

	protected $db = null;
	
	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\FLOW3\Configuration\ConfigurationManager
	 */
	protected $configurationManager; 
	
	public function initializeAction () {
		if ($_SERVER['HTTP_HOST'] == "comgate.worldboxglobal.ch") {
			ComSysLogger::getLogger()->log("USING PROD WEB SERVICE");
			self::$CIM_SYS_URL 			= "http://cimsys.worldbox.ch/view_partyIdent.cfm?ID=";
			self::$WEB_SERVICE_WSDL 	= "https://secure.worldbox.ch/ws_comgate/dataGateway.cfc?wsdl";
			
			
			// I know its ugly. Direct db access for webservice cache (below)
			$this->settings = $this->configurationManager->getConfiguration(\TYPO3\FLOW3\Configuration\ConfigurationManager::CONFIGURATION_TYPE_SETTINGS, 'TYPO3.FLOW3');
	 		$host 			= $this->settings['persistence']['backendOptions']['host'];
			$user 			= $this->settings['persistence']['backendOptions']['user'];
	 		$password 		= $this->settings['persistence']['backendOptions']['password'];
	 		$dbname 		= $this->settings['persistence']['backendOptions']['dbname'];
	 	
			
			
			$this->db = mysql_connect($host, $user, $password);
			if (!$this->db) {
				ComSysLogger::getLogger()->log("Failed to connect to the database");
			}
			if (!mysql_select_db($dbname, $this->db)) {
				ComSysLogger::getLogger()->log("Failed to select database " . $dbname);
			}

		} else {
			ComSysLogger::getLogger()->log("USING DEV WEB SERVICE");
		}
	}
	
	/**
	 * Index action
	 *
	 * @return void
	 */
	public function indexAction() {
		
		$linkProgSections = $this->linkProgSectionRepository->getWinMatchCompLinkProgSections ();

		// Load all programs for company relation mapping (map a company with a WIN to the parent company)
		$dbProgs = array();
		foreach ($linkProgSections as $linkProgSection) {
			$dbProgs[] = $this->dbProgRepository->findByIdentifier($linkProgSection->getDbProg()->getTheid());
		}
		
		// Load all programs for WIN matching (The companies do not have a WIN yet. It has either to be 
		// assigned to an existing company in cimsys (WIN) or a new company in cimsys needs to be created).
		$dbProgsNoWinArray = $this->dbProgRepository->getDbProgNoWinCases();
		$dbProgsNoWin = array();
		foreach ($dbProgsNoWinArray as $dbProgNoWinArray) {
			$dbProgsNoWin[] = $this->dbProgRepository->findByIdentifier($dbProgNoWinArray['THEID']);
		}
		
		
		$this->view->assign("dbProgs", $dbProgs);
		$this->view->assign("dbProgsNoWin", $dbProgsNoWin);
	}
	
	
	/**
	 * Used for developement to restore test data.
	 * 
	 */
	public function restoreTestDataAction () {
		$this->dbComgateCompRepository->restoreTestData();
		$this->redirect("index");
	}

	
	
	/**
	 * Given a dbProg the method loads the Header part of the navigation that shows
	 * the prog and the first dbComgate object to handle
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 */
	public function loadProgAction (\Worldbox\ComSys\Domain\Model\DbProg $dbProg) {
	
		$time = time();
	
		// Show number of cases in the program				
		$numberOfOpenCases 	= $this->dbProgRepository->getNumberOfOpenWinMapCases($dbProg);
	
		// Get list of db_comage ids that have db_comgate_comp match_status = 5
		$comgateIds 		= $this->dbProgRepository->getDbComgateCases($dbProg);
		
		
		$dur = time() - $time;
		
		// Get country code if set in sesssion. Otherwise use ALL
		$countryCode 		= "ALL";
		if (isset($_COOKIE['selectedCountry'])) {
			$countryCode 		= $_COOKIE['selectedCountry'];
		}
		
		//enhanceDbComgateListWithIdentInformation
		// Fetch all comgate objects by their id and store them in list
		$dbComgateList = array();
		foreach ($comgateIds as $comgateId) {
			
			$dbComgate			 	= $this->dbComgateRepository->findByIdentifier($comgateId["THEID"]);
			
			// Just in case there is no WIN defined
			if (!is_string($dbComgate->getWin()) || strlen($dbComgate->getWin())< 10) {
				ComSysLogger::getLogger()->log("There is no WIN defined for dbComgate object with id " . $comgateId, ComSysLogger::SEVERITY_ERROR);
				continue;
			}
			$dbComgateList[] = $dbComgate;
		}
		
		$dbComgateList = $this->enhanceDbComgateListWithIdentInformation($dbComgateList);
		
		$numberOfMainCompanies 	= 0;
		$matchingDbComgate  	= array();
		foreach ($dbComgateList as $dbComgate) {
						
			if ($countryCode != "ALL" && $dbComgate->getDbIdentCountryCode() != $countryCode) continue;
			
			if ($dbComgate == false) continue;
			$numberOfMainCompanies++;	
			$matchingDbComgate[] 	= $dbComgate;
		
		}

		$this->destroySoapSession();
		
		$this->view->assign("numberOfEntries", $numberOfOpenCases);
		
		// Add country for filter
		$this->view->assign ("countries", $this->linkCityCountryRepository->getCountryList());
		$this->view->assign("dbComgateList", $matchingDbComgate);
		$this->view->assign("progCountryCode", $countryCode);
		$this->view->assign("dbProg", $dbProg);	
		$this->destroySoapSession();
		
		ComSysLogger::getLogger()->log("Total Duration: " . $this->totalWebServiceTime);
	}
		
	
	
	/**
	 * Performs a company search via web service. Is used to find potential matches
	 * for a dbComgateComp object.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp
	 * @param string $dbComgateComp
	 * @param string $dbComgateComp
	 */
	public function searchAction ($dbComgateComp, $searchName, $countryCode) {
		
		//$dbComgateComp = $this->dbComgateCompRepository->findByIdentifier($dbComgateCompId);
		//$selectedLinkCityCountry = $this->linkCityCountryRepository->getByCountryCode($countryCode);
		ComSysLogger::getLogger()->log($dbComgateComp->getTheid());
	
		ComSysLogger::getLogger()->log("Country Code " . $countryCode);
		
		$matchList  = $this->doWebserviceSearch($searchName, $countryCode);
		$this->destroySoapSession();
		$this->view->assign("matchList", $matchList);
		$this->view->assign("dbComgateComp", $dbComgateComp);
	}
	
	
	/**
	 * Loads the mask for the WIN match of a single dbComgateComp object given a parent
	 * company (DbComgate object).
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 */
	public function dbComgateCompListAction (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate) {
		

		$dbProg				= $dbComgate->getDbProg();	
		$dbComgateComp 		= $this->dbComgateCompRepository->getNextDbComgateCompByParentCompany($dbComgate);
		$numberOfCases 		= $this->dbComgateCompRepository->getNumberOfCasesByDbComgate($dbComgate);
		
	
		$dbProg = $this->dbProgRepository->findByIdentifier($dbProg->getTheid());
		$this->destroySoapSession();
		$this->redirect("loadDbComgateCompForMatch", null, null, array("dbProg" => $dbProg, "dbComgateComp" => $dbComgateComp, "dbComgate" => $dbComgate, "selectedCountry" => "XX", "numberOfCases" => $numberOfCases));
	}
	
	/**
	 * Returns a list of dbComgateComp objects matching the given criteria.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 * @param string $country
	 */
	public function searchDbComgateCompByCountryAction ($dbProg, $countryCode) {
			
		// Get all dbComgateComp having dbComgateData with dataelement = identAddressCountryCode and verifiedToken = $countryCode
		ComSysLogger::getLogger()->log("Search Comgate by CountryCode " . $countryCode);	
		$dbComgateComp 		= $this->dbComgateCompRepository->getNextDbComgateCompByCountry($dbProg->getProgCode(), $countryCode);
		
		if ($dbComgateComp == null) {
			return "<b>Nothing to do for that Country</b>";
		}
		$numberOfCases 		= $this->dbComgateCompRepository->getNumberOfCasesByCountry($dbProg->getProgCode(), $countryCode);
		$dbComgate			= $dbComgateComp->getDbComgate();
		$dbProg = $this->dbProgRepository->findByIdentifier($dbProg->getTheid());
		//return $dbProg->getTheid();
		$this->destroySoapSession();
		$this->redirect("loadDbComgateCompForMatch", null, null, array("dbProg" => $dbProg, "dbComgateComp" => $dbComgateComp, "dbComgate" => $dbComgate, "selectedCountry" => $countryCode, "numberOfCases" => $numberOfCases));
		
	}
	


	/**
	 * Given a dbComageComp object the method loads the next one to be edited. Only those that
	 * are related to the same DbComgate object are considered.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp
	 */
	public function loadNextDbComgateCompAction (\Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp) {

		$dbComgate 			= $dbComgateComp->getDbComgate();
		$dbProg				= $dbComgate->getDbProg();
		$nextDbComgateComp 	= $this->dbComgateCompRepository->getNextDbComgateCompByParentCompany($dbComgate, $dbComgateComp->getTheid());
		$numberOfCases 		= $this->dbComgateCompRepository->getNumberOfCasesByDbComgate($dbComgate);
		if ($nextDbComgateComp == null) { // There is no next. Just return the last which is the one given as parameter.
			// There is no next -> load first
			$nextDbComgateComp 	= $this->dbComgateCompRepository->getNextDbComgateCompByParentCompany($dbComgate);
		} 
		
		if ($nextDbComgateComp == null) {
			return "done";
		}
		$this->destroySoapSession();
		$dbProg = $this->dbProgRepository->findByIdentifier($dbProg->getTheid());
		$this->redirect("loadDbComgateCompForMatch", null, null, array("dbProg" => $dbProg, "dbComgateComp" => $nextDbComgateComp, "dbComgate" => $dbComgate, "selectedCountry" => "XX", "numberOfCases" => $numberOfCases));
			
	}
	
	
	/**
	 * Given a dbComageComp object the method loads the next one to be edited.Only those that
	 * are related to the same DbComgate object are considered.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp
	 */
	public function loadPreviousDbComgateCompAction (\Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp) {

		$dbComgate 	= $dbComgateComp->getDbComgate();
		$dbProg		= $dbComgate->getDbProg();
		$previousDbComgateComp = $this->dbComgateCompRepository->getPreviousDbComgateCompByParentCompany($dbComgate, $dbComgateComp->getTheid());
		$numberOfCases 		= $this->dbComgateCompRepository->getNumberOfCasesByDbComgate($dbComgate);
		if ($previousDbComgateComp == null) { // There is no next. Just return the last which is the one given as parameter.
			$previousDbComgateComp = $dbComgateComp;
		}
		
		if ($previousDbComgateComp == null) {
			return "done";
		}
		$this->destroySoapSession();
		$dbProg = $this->dbProgRepository->findByIdentifier($dbProg->getTheid());
		$this->redirect("loadDbComgateCompForMatch", null, null, array("dbProg" => $dbProg, "dbComgateComp" => $previousDbComgateComp, "dbComgate" => $dbComgate, "selectedCountry" => "XX", "numberOfCases" => $numberOfCases));
	}
	
	
	/**
	 * Loads the next dbComgateComp to be edited in case we are in country search mode.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp
	 * @param string $countryCode
	 */
	public function loadNextDbComgateCompByCountryAndProgAction ($dbProg, $dbComgateComp, $countryCode) {
		
		$nextDbComgateComp 	= $this->dbComgateCompRepository->getNextDbComgateCompByCountry($dbProg->getProgCode(), $countryCode, $dbComgateComp->getTheid());
		$numberOfCases 		= $this->dbComgateCompRepository->getNumberOfCasesByCountry($dbProg->getProgCode(), $countryCode);
		if ($nextDbComgateComp == null) { // There is no next. Just return the last which is the one given as parameter.
			// There is no next -> load first
			$nextDbComgateComp 	= $this->dbComgateCompRepository->getNextDbComgateCompByCountry($dbProg->getProgCode(), $countryCode);
		} 
		
		if ($nextDbComgateComp == null) {
			return "done";
		}
		$this->destroySoapSession();
		$dbComgate = $nextDbComgateComp->getDbComgate();
		$dbProg = $this->dbProgRepository->findByIdentifier($dbProg->getTheid());
		$this->redirect("loadDbComgateCompForMatch", null, null, array("dbProg" => $dbProg, "dbComgateComp" => $nextDbComgateComp, "dbComgate" => $dbComgate, "selectedCountry" => $countryCode, "numberOfCases" => $numberOfCases));
			
	}
	
	/**
	 * Loads the previous dbComgateComp to be edited in case we are in country search mode.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp
	 * @param string $countryCode
	 */
	public function loadPreviousDbComgateCompByCountryAndProgAction ($dbProg, $dbComgateComp, $countryCode) {
		
		$previousDbComgateComp 	= $this->dbComgateCompRepository->getPreviousDbComgateCompByCountry($dbProg->getProgCode(), $countryCode, $dbComgateComp->getTheid());
		$numberOfCases 		= $this->dbComgateCompRepository->getNumberOfCasesByCountry($dbProg->getProgCode(), $countryCode);
		if ($previousDbComgateComp == null) { // There is no next. Just return the last which is the one given as parameter.
			// There is no next -> load first
			$previousDbComgateComp 	= $dbComgateComp;
		} 
		
		if ($previousDbComgateComp == null) {
			return "done";
		}
		$this->destroySoapSession();
		$dbComgate = $previousDbComgateComp->getDbComgate();
		$dbProg = $this->dbProgRepository->findByIdentifier($dbProg->getTheid());
		$this->redirect("loadDbComgateCompForMatch", null, null, array("dbProg" => $dbProg, "dbComgateComp" => $previousDbComgateComp, "dbComgate" => $dbComgate, "selectedCountry" => $countryCode, "numberOfCases" => $numberOfCases));
			
	}
	
	/**
	 * Returns a list of dbComgateComp objects matching the given criteria.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 * @param string $country
	 */
	public function searchDbComgateAction ($dbProg, $countryCode) {
		
		// Get all DbComgate objects of the program in the given country.
		$comgateIds 					= $this->dbProgRepository->getDbComgateCases($dbProg);
		$matchingDbComgate  			= array();
		$_SESSION['selectedCountry'] 	= $countryCode;
		setcookie ( "selectedCountry", $countryCode);
		$numberOfMainCompanies = 0;
		
		// Enhance all dbComgate elements through webservice with IDENT information
		$dbComgateList = array();
		foreach ($comgateIds as $comgateId) {
			
			$dbComgate			 	= $this->dbComgateRepository->findByIdentifier($comgateId["THEID"]);
			
			// Just in case there is no WIN defined
			if (!is_string($dbComgate->getWin()) || strlen($dbComgate->getWin())< 10) {
				ComSysLogger::getLogger()->log("There is no WIN defined for dbComgate object with id " . $comgateId, ComSysLogger::SEVERITY_ERROR);
				continue;
			}
			$dbComgateList[] = $dbComgate;
		}
		$dbComgateList = $this->enhanceDbComgateListWithIdentInformation($dbComgateList);
		
		// Filter by selected country
		foreach ($dbComgateList as $dbComgate) {
				
			// Filter all that do not have dbIdent contry code like the one we are looking for
			if ($countryCode != "ALL" && $dbComgate->getDbIdentCountryCode() != $countryCode) continue;
			$numberOfMainCompanies++;	
			$matchingDbComgate[] 	= $dbComgate;
			
		}
		$this->destroySoapSession();
		$this->view->assign("dbComgateList", $matchingDbComgate);
	}


	
	
	/**
	 * Creates a new WIN with the given company information.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp
	 */
	public function openNewCompanyAction (\Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp) {
		
		$dbComgateCompDataElements 	= $this->getGetDataElementsForDbComgateComp($dbComgateComp);
		$identName 					= $dbComgateCompDataElements["identName"];
		$identAddressCountryCode 	= $dbComgateCompDataElements["identAddressCountryCode"];
		if (!isset($dbComgateCompDataElements["predicateLegalForm"]) || $dbComgateCompDataElements["predicateLegalForm"] == "N/A") {
			$predicateLegalForm = "XXX";
		} else {
			$predicateLegalForm 		= $dbComgateCompDataElements["predicateLegalForm"];
		}
		$predicateCompanyStatus 	= $dbComgateCompDataElements["predicateCompanyStatus"];
		
		
		ComSysLogger::getLogger()->log("Open new company " . $identName . " " . $identAddressCountryCode . " " . $dbComgateComp->getRoleType());
		$arguments = array($identAddressCountryCode, $identName, $predicateLegalForm, "", $predicateCompanyStatus,"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0);
		
		$win = $this->doSoapRequest("insertIntoIdent", $arguments);
		ComSysLogger::getLogger()->log("Created new WIN " . $win);
			
		$dbComgateComp->setWin($win);
		$dbComgateComp->setMatchStatu(9);
		$this->dbComgateCompRepository->update($dbComgateComp);
		$this->persistenceManager->persistAll();
		$this->destroySoapSession();
		return $win;
	}
	
	
	
	/**
	 * Sets the WIN for a dbComgateComp element. 
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp
	 * @param string $win 
	 */
	public function setWinForDbComgateCompAction (\Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp, $win) {
		
		$dbComgateComp->setWin($win);
		$dbComgateComp->setMatchStatu(9);
		
		$this->dbComgateCompRepository->update($dbComgateComp);
		$this->persistenceManager->persistAll();
		$this->destroySoapSession();
		return "ok";
	}
	
	
	/**
	 * Inactivates the relation between the given dbComgateComp and the main Company
	 * it is related to.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp
	 */
	public function inactivateRelationAction (\Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp) {
		
		// Delete dbComgateData elements of the dbComgateComp object
		$dbComgateDataElements = $dbComgateComp->getDbComgateData();
		foreach ($dbComgateDataElements as  $dbComgateData) {
			$this->dbComgateDataRepository->remove($dbComgateData);
		}
		$dbComgateComp->setMatchStatu(10);
		$dbComgateComp->setVerifiedStatus(10);
		$dbComgateComp->setInactive(1);
		$this->dbComgateCompRepository->update($dbComgateComp);
		$this->persistenceManager->persistAll();
		return "ok";
	}
	
	
	/**
	 * =======================================================
	 * Helper functions
	 * =======================================================
	 */
	
	protected $apiSessionKey 	= null;
	
	protected $soapClient 		= null;
	
	public function getApiSessionKey () {
		
		if ($this->apiSessionKey == null) {
	
			$this->soapClient  		= new \SoapClient(self::$WEB_SERVICE_WSDL, array("trace" => 1, "exception" => 0,'connection_timeout'=>500,'cache_wsdl'=>  WSDL_CACHE_DISK));
			$this->apiSessionKey 	= $this->soapClient->__soapCall("openSession", array("user" => self::WEB_SERVICE_USER, "passowrd" => self::WEB_SERVICE_PASS), null, null); 
		}
		return $this->apiSessionKey;
	}
	
	public function getSoapClient () {
		$this->getApiSessionKey();
		return $this->soapClient;
	}
	
	/**
	 * Use this method if you want to perform a soap request to the API.
	 * 
	 * @param string $method
	 * @param array $arguments
	 */
	public function doSoapRequest ($method, $arguments = array()) {
		
		$soapClient 				= $this->getSoapClient();

		// Put session key at beginning of arguments array (!!! Order matters)
		array_unshift ($arguments , $this->getApiSessionKey());
		$startTime = microtime(true);
		try {
			
			//ComSysLogger::getLogger()->log("WebService Args: \n" . print_r($arguments, true), ComSysLogger::SEVERITY_NOTICE);
			$result 	= $soapClient->__soapCall($method, $arguments, null, null); 
			//ComSysLogger::getLogger()->log("WebService Response: \n" . print_r($result, true), ComSysLogger::SEVERITY_NOTICE);
		} catch (Exception $e) {
			ComSysLogger::getLogger()->log($e->getMessage(), ComSysLogger::SEVERITY_ERROR);
			ComSysLogger::getLogger()->log($method, ComSysLogger::SEVERITY_ERROR);
			ComSysLogger::getLogger()->log(print_r($arguments, true), ComSysLogger::SEVERITY_ERROR);
			
			return false;
		}
		$endTime 	= microtime(true);
		$duration 	= $endTime-$startTime;
		$this->totalWebServiceTime += $duration;
		
		
		return $result;
	}
	
	
	/**
	 * Should be called at end of soap interaction.
	 * 
	 */
	public function destroySoapSession () {
		if ($this->soapClient == null) return false;
		
		
		$result 	= $this->soapClient->__soapCall("closeSession", array("SessionKey" => $this->getApiSessionKey()), null, null); 
		$this->apiSessionKey = null;
		ComSysLogger::getLogger()->log("Destroy session " . print_r($result, true));
	}
	
	

	/**
	 * Given a name and a country the method calls the webservice to get a list
	 * of exiting companies with similar or equal name in the same country.
	 * 
	 * @param string $name
	 * @param string $country
	 */
	private function doWebserviceSearch ($name, $country) {
		
		$matchList  = array(); // Holds the list of matches
		try {
			
			// Do a soap request-
			$result = $this->doSoapRequest("getCompanyMatch", array("name" => $name, "country" => $country, "lang" => "EN"));
	
			if (is_object($result) && isset($result->data) && is_array($result->data)) {
				
				ComSysLogger::getLogger()->log("Number of Matches for '" . $name . "' '" . $country . "': " . count($result->data));
				foreach ($result->data as $match) {
					
					$matchList[] = array (
						"win" 		=> $match[0],
						"country" 	=> $match[5],
						"legalform" => $match[2],
						"rating"	=> $match[6],
						"name"		=> $match[4],
						"city"		=> $match[1],
						"status"	=> $match[3],
						"cimSys"	=> self::$CIM_SYS_URL . $match[0]
					);
				}
			}
			
			// Close session
			$this->destroySoapSession();
		} catch (Exception $e) {
			ComSysLogger::getLogger()->log($e->getMessage(), ComSysLogger::SEVERITY_ERROR);
		}
		return $matchList;
	}
	
	
	/**
	 * Given a dbComgateComp object the method returns the html needed to edit
	 * it. What object to be edited is determined by other actions this one is
	 * just used to return it.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 * @param string $selectedCountry
	 * @param int $numberOfCases
	 */
	public function loadDbComgateCompForMatchAction (\Worldbox\ComSys\Domain\Model\DbProg $dbProg, $dbComgateComp, $dbComgate = null, $selectedCountry = null, $numberOfCases) {
		
		
		
		if ($selectedCountry != "XX") {
			$countrySearch 				= true;
		} else {
			$countrySearch 				= false;
			$selectedCountry			= false;
		}
		
		// Determine name and such from the db_comgate_data elements of the db_comgate_comp record
		$dbComgateDataElements 		= $dbComgateComp->getDbComgateData();
		ComSysLogger::getLogger()->log("Number of data elements: " . count($dbComgateDataElements));
		
		
		$dbComgateDataElements 		= $this->getGetDataElementsForDbComgateComp($dbComgateComp);
		
		
		$identName 					= $dbComgateDataElements["identName"];
		$identAddressCity 			= $dbComgateDataElements["identAddressCity"];
		$identAddressCountryCode 	= $dbComgateDataElements["identAddressCountryCode"];
		$predicateLegalForm 		= $dbComgateDataElements["predicateLegalForm"];
		$predicateCompanyStatus 	= $dbComgateDataElements["predicateCompanyStatus"];
		$win 						= $dbComgateDataElements["win"];
		$regOfficeRegId				= $dbComgateDataElements["regOfficeRegId"];
		
		
		//@todo: Remove this -> just for testing
		if ($identAddressCountryCode == "N/A") {
			$identAddressCountryCode = "CH";
		}
		ComSysLogger::getLogger()->log("Country Code " . $identAddressCountryCode );
				
		$countryList				= $this->linkCityCountryRepository->getCountryList();
		
		if ($identAddressCountryCode != "N/A") {
			
			foreach ($countryList as $country) {
				
				if ($country['country_code'] == substr($identAddressCountryCode, 0, 2)) {
					$selectedCountry = $country;
					break;
				}
			}
		} else {
			$countryCode = "XY"; // Country code not known
		}
		
		
		// Fetch the match list using the country and company name:
		// Intelligent method for serach string preparation is required
		// This one just removes last word (if contains more than one)
		
		$words 			= explode(" ", $identName);
		$searchString   = $words[0];
		foreach ($words as $key => $word) {
			if ($key == 0) continue; 
			if ($key == count($words) - 1) break;
			$searchString .= " " . $word;
		}
		
		
		$countryCodeForSearch = "CH";
		if ($selectedCountry != false) {
			$countryCodeForSearch = $selectedCountry['country_code'];
		}
		ComSysLogger::getLogger()->log("Search String " . $searchString);
		if (!is_string($searchString) || strlen($searchString) == 0) {
			$searchString = "test"; // WebService gives error in case of no serach string defined.
		}
		//$matchList 			= $this->doWebserviceSearch($searchString, $countryCodeForSearch);
		$matchList = array();
		//@todo: Implement this 
		//$this->view->assign("winOpenAllowed", $this->checkAllowedToOpenWin($identAddressCountryCode, $predicateLegalForm));
		$this->view->assign("winOpenAllowed", true);
		
		//@todo: Soap call returns error -> want to enhance dbComgate to show additional information
		$this->destroySoapSession();
		$dbComgate = $this->cachedEnhanceDbComgateWithIdentInformation($dbComgate);
		
		
		
		$this->view->assign("countrySearch", $countrySearch);
		$this->view->assign("identName", $identName);
		$this->view->assign("identAddressCity", $identAddressCity);
		$this->view->assign("identAddressCountryCode", $identAddressCountryCode);
		$this->view->assign("predicateLegalForm", $predicateLegalForm);
		$this->view->assign("predicateCompanyStatus", $predicateCompanyStatus);
		$this->view->assign("win", $win);
		$this->view->assign("regOfficeRegId", $regOfficeRegId);
		
		$this->view->assign("countries", $countryList);
		$this->view->assign("dbComgateComp", $dbComgateComp);
		$this->view->assign("numberOfCases", $numberOfCases);
		$this->view->assign("matchList", $matchList);
		$this->view->assign("dbProg", $dbProg);
		$this->view->assign("selectedCountry", $selectedCountry);
		$this->view->assign("dbComgate", $dbComgate);
	
	}
	
	
	/**
	 * The method returs an array containing the most important data elements
	 * and ist values.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateComp $dbComgateComp
	 * @return array
	 */
	public function getGetDataElementsForDbComgateComp ($dbComgateComp) {
		
		$dbComgateDataElements 		= $dbComgateComp->getDbComgateData();
		
		$dataElements 				= array();
		$dataElements['identName'] 					= "N/A";
		$dataElements['identAddressCity'] 			= "N/A";
		$dataElements['identAddressCountryCode'] 	= "N/A";
		$dataElements['predicateLegalForm']	 		= "N/A";
		$dataElements['predicateCompanyStatus'] 	= "N/A";
		$dataElements['win'] 						= "N/A";
		$dataElements['regOfficeRegId']				= "N/A";
		
		foreach ($dbComgateDataElements as $dbComgateDataElement) {
			
			$dataElement = $dbComgateDataElement->getSectionAndSpecifyer();
			ComSysLogger::getLogger()->log("DataElement " . $dataElement . " " . $dbComgateDataElement->getTheid() . " " . $dbComgateDataElement->getVerifiedToken());
			// @todo: Think I have to use getVerifiedToken() instead of getOriginal() -> For all of the parameters??
			if ($dataElement == "Ident/Name")  						$dataElements['identName']					= $dbComgateDataElement->getVerifiedToken();
			if ($dataElement == "Ident/AddressCity")  				$dataElements['identAddressCity'] 			= $dbComgateDataElement->getVerifiedToken();
			if ($dataElement == "Ident/AddressCountry_code")  		$dataElements['identAddressCountryCode'] 	= $dbComgateDataElement->getVerifiedToken();
			if ($dataElement == "Predicate/Legalform_code") 	 	$dataElements['predicateLegalForm']		 	= $dbComgateDataElement->getVerifiedToken();
			if ($dataElement == "Predicate/Companystatus_code")  	$dataElements['predicateCompanyStatus'] 	= $dbComgateDataElement->getVerifiedToken();
			if ($dataElement == "Ident/WIN")  						$dataElements['win'] 						= "N/A"; // Win is not availabe -> thats why we do the whole thing
			if ($dataElement == "RegOffice/RegId")  				$dataElements['regOfficeRegId'] 			= $dbComgateDataElement->getVerifiedToken();
		}
		
		ComSysLogger::getLogger()->log("DataElements " . print_r($dataElement, true));
		return $dataElements;
	}
	
	
	
	
	/**
	 * Given a dbComgateComp element the method calls the webservice with the WIN
	 * defined in the dbComgate object to get more information:
	 * - Company Name
	 * - Company Country
	 * 
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 * @return \Worldbox\ComSys\Domain\Model\DbComgate|boolean
	 */
	public function enhanceDbComgateWithIdentInformation (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate) {
		
		ComSysLogger::getLogger()->log("enhace Db comgate");
		if (!is_object($dbComgate)) return false;
		
		try {
			
			$result = $this->doSoapRequest("getNameCountry", array("win" => $dbComgate->getWin())); 
			
			ComSysLogger::getLogger()->log("Enhacing db Comgate with id " . $dbComgate->getTheid() . " and WIN " . $dbComgate->getWin() . "\n" . print_r($result, true));

			if (is_object($result) && isset($result->data) && is_array($result->data)) {
				$dbIdentName = "Unkown";
				if (isset($result->data[0][0])) {
					$dbIdentName 		= $result->data[0][0];
				}
				$dbIdentCountryCode = "N/A";
				if (isset($result->data[0][2])) {
					$dbIdentCountryCode = $result->data[0][2];
				}
				
				
				$dbComgate->setDbIdentName($dbIdentName);
				$dbComgate->setDbIdentCountryCode($dbIdentCountryCode);
				return $dbComgate;
			}
			
		} catch (Exception $e) {
			return false;
		}
		return false;
	}
	
	
	/**
	 * Does the same as enhanceDbComgateWithIdentInformation() but instead of firing a soap
	 * request directly it first checks whether the desired informaiton is stored in the
	 * TEMP_WEBSERVICE_CACHE table.
	 * 
	 * @param unknown_type $dbComgate
	 */
	public function cachedEnhanceDbComgateWithIdentInformation (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate) {
			
		$row = $this->getCachedIdentInformation($dbComgate);
		if (is_array($row)) { // Cached version available -> use it
			
			$dbComgate->setDbIdentName($row['INDENT_NAME']);
			$dbComgate->setDbIdentCountryCode($row['COUNTRY_CODE']);
		} else { // Not cached -> fetch via webservice and cache it
			$dbComgate = $this->enhanceDbComgateWithIdentInformation($dbComgate);
			$this->createIdentCacheRecord($dbComgate);
		}
		return $dbComgate;
	}
	
	/**
	 * Returns an array with ident information if in cache or null if not in cache.
	 * 
	 * @param unknown_type $dbComgate
	 */
	private function getCachedIdentInformation (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate) {
		$query 			= "SELECT * FROM TEMP_WEBSERVICE_CACHE WHERE WIN = '" . $dbComgate->getWin() . "'";
		$result		 	= mysql_query($query, $this->db);
		if (mysql_num_rows($result) == 1) {
			$row 	= mysql_fetch_assoc ($result);
			return $row;
		}
		return null;
	}
	
	/**
	 * Writes the ident information in the dbComgate object to the cache.
	 * If no ident info is there the method just returns false.
	 * 
	 * @param unknown_type $dbComgate
	 */
	private function createIdentCacheRecord (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate) {
		
		if (!is_string($dbComgate->getDbIdentName()) || !is_string($dbComgate->getDbIdentCountryCode())) return false;
		$query = "INSERT INTO TEMP_WEBSERVICE_CACHE (
					WIN,
					INDENT_NAME,
					COUNTRY_CODE
				)
				VALUES (
					'" . $dbComgate->getWin() . "' , 
					'" . mysql_real_escape_string($dbComgate->getDbIdentName(), $this->db) . "', 
					'" . $dbComgate->getDbIdentCountryCode() . "'
					);";

		$result = mysql_query($query);
	}
	
	/**
	 * In case the ident infomration of the given dbComgate (by win) is not 
	 * cached this function does it.
	 * 
	 * @param unknown_type $dbComgate
	 */
	private function cacheIdentInformation (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate) {
		
		// Reject if no ident info present
		if (!is_string($dbComgate->getDbIdentName()) || !is_string($dbComgate->getDbIdentCountryCode())) return false;
		
		if ($this->getCachedIdentInformation($dbComgate) == null) {
			$this->createIdentCacheRecord($dbComgate);
		}		
	}
	
	
	
	/**
	 * Due to the problem that enhncing multiple dbComgate elements with webservice
	 * information is a performance problem this method enhnces a while list
	 * of dbComgate objects.
	 * 
	 * @param array $dbComgateList
	 * @return array
	 */
	public function enhanceDbComgateListWithIdentInformation ($dbComgateList) {
		
		
		// Create a comma separated list of all wins
		$notCached = array();
		$winList = null;
		$numberOfWins = 1;
		foreach ($dbComgateList as $dbComgate) {
			
			$cachedInfo = $this->getCachedIdentInformation($dbComgate);
			if (is_array($cachedInfo)) {
				$dbComgate->setDbIdentName($cachedInfo['INDENT_NAME']);
				$dbComgate->setDbIdentCountryCode($cachedInfo['COUNTRY_CODE']);
				continue;
			}
			$notCached[] = $dbComgate;
			$win = $dbComgate->getWin();
			if ($winList == null) {
				$winList = $win;
			} else {
				$winList .= "," . $win;
			}
			$numberOfWins++;
		}
		
		// We are done -> every thing was cached
		if (count($notCached) == 0) return $dbComgateList;
		
		ComSysLogger::getLogger()->log("Number of WINs to enhance: " . $numberOfWins);
		
		if ($winList != null) {
			$result = $this->doSoapRequest("getNameCountry", array("win" => $winList)); 
			
			if (is_object($result) && isset($result->data) && is_array($result->data)) {
			
				
				foreach ($notCached as $dbComgate) {
				
					$win 					= $dbComgate->getWin();
					$enhancementSuccessfull = false;
					foreach ($result->data as $identInfo) {
						
						$dbIdentName = "Unkown";
						if (isset($identInfo[0])) {
							$dbIdentName 		= $identInfo[0];
						}
						$dbIdentCountryCode = "N/A";
						if (isset($identInfo[2])) {
							$dbIdentCountryCode = $identInfo[2];
						}
						
						$identWin = "N/A";
						if (isset($identInfo[3])) {
							$identWin = $identInfo[3];
						}
						
						if ($win == $identWin) {
							$enhancementSuccessfull = true;
							ComSysLogger::getLogger()->log("Enhancing DbComgate with win " . $dbComgate->getWin() . " with: " . $dbIdentName . " " . $dbIdentCountryCode);
							$dbComgate->setDbIdentName($dbIdentName);
							$dbComgate->setDbIdentCountryCode($dbIdentCountryCode);
							$this->cacheIdentInformation($dbComgate);
							break;
						}
						
					}
				
				}
			} else {
				ComSysLogger::getLogger()->log("Problem with dbComgateList enhancement: Soap call did not return same number of entries as in comgate list", ComSysLogger::SEVERITY_ERROR);
			}

		}
		
		return $dbComgateList;
	}
	
	
	/** 
	 * Chechs via WebService whether it is allowed to open a WIN.
	 * Ask what the legalForm value has to be.
	 * 
	 *
	 */
	public function checkAllowedToOpenWin ($countryCode, $legalForm) {
		
		
		$arguments = array();
		$arguments['country_code']	= $countryCode;
		$arguments['legalform'] 	= $legalForm;
	
		$answer = $this->doSoapRequest("getRegistrationRequired", $arguments);
		ComSysLogger::getLogger()->log("WIN Open allowed: " . print_r($answer, true));
		if ($answer == 1) {
			return false;
		}
		return true;
	}
	
	/**********************************************************************
	 * Actions for to handle the programs with no WIN
	 **********************************************************************
	 */
	
	
	/**
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 */
	public function loadProgNoWinAction (\Worldbox\ComSys\Domain\Model\DbProg $dbProg) {
		$numberOfOpenCases 	= $this->dbProgRepository->getNumberOfOpenWinMapCases($dbProg);
		
		$this->view->assign("dbProg", $dbProg);
		$this->view->assign("numberOfEntries", $numberOfOpenCases);
		
		ComSysLogger::getLogger()->log("Total Duration: " . $this->totalWebServiceTime);
	}
	
	
	/**
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 */
	public function loadDbComgateForMatchNoWinAction (\Worldbox\ComSys\Domain\Model\DbProg $dbProg, \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate = null) {
		
		// Get Number of cases
		$numberOfCases = $this->dbComgateRepository->getNumberOfMatchCasesNoWin($dbProg->getProgCode())[0][0];
		
		
		// Get the right dbComgate to display
		$currentDbComgate = null;
		$nextDbComgate = null;
		$previousDbComgate = null;
		
		if ($dbComgate == null) { // Initial load (first)
			$currentDbComgate = $this->dbComgateRepository->getFirstNoWin($dbProg->getProgCode());
			if ($currentDbComgate != null) {
				$nextDbComgate = $this->dbComgateRepository->getNextNoWin($currentDbComgate, $dbProg->getProgCode());
			}
		} else {
			$currentDbComgate = $dbComgate;
			$nextDbComgate = $this->dbComgateRepository->getNextNoWin($dbComgate, $dbProg->getProgCode());
			$previousDbComgate = $this->dbComgateRepository->getPreviousNoWin($dbComgate, $dbProg->getProgCode());
		}
		
		
		
		$this->view->assign("numberOfEntries", $numberOfCases);
		$this->view->assign("dbProg", $dbProg);
		$this->view->assign("currentDbComgate", $currentDbComgate);
		$this->view->assign("nextDbComgate", $nextDbComgate);
		$this->view->assign("previousDbComgate", $previousDbComgate);
		$this->view->assign ("countries", $this->linkCityCountryRepository->getCountryList());
		
		
		// Set default search field value dbComgate
		$searchDefault = "";	
		$dbComgateDataName = $this->getDbComgateDataName($currentDbComgate);
		if ($dbComgateDataName != false && $dbComgateDataName->getSerial() == 1) {
			$searchDefault = $dbComgateDataName->getVerifiedToken();
		} 
		
		// Set default country value dbComgate
		$countryDefault = "";
		$dbComgateDataCountry = $this->getDbComgateDataIdentOrStandard("Address", "Country_code", $currentDbComgate);
		
		if ($dbComgateDataCountry != false && $dbComgateDataCountry->getSerial() == 1) {
			$countryDefault = $dbComgateDataCountry->getVerifiedToken();
		} 
		
		$this->view->assign("searchDefault", $searchDefault);
		$this->view->assign("countryDefault", $countryDefault);
		

		// Set current db comgate fields
		$city = "";
		$legalform = "";
		$status = "";
		
		$dbComgateDataCity = $this->getDbComgateDataIdentOrStandard("Address", "City", $currentDbComgate);
		if ($dbComgateDataCity != false && $dbComgateDataCity->getSerial() == 1) {
			$city = $dbComgateDataCity->getVerifiedToken();
		}
		
		$dbComgateDataLegalform = $currentDbComgate->getDbComgateDataWithSectionAndSpecifier("Predicate", "Legalform_code");
		if ($dbComgateDataLegalform != false && $dbComgateDataLegalform->getSerial() == 1) {
			$legalform = $dbComgateDataLegalform->getVerifiedToken();
		}
		
		$dbComgateDataStatus = $currentDbComgate->getDbComgateDataWithSectionAndSpecifier("Predicate", "Companystatus_code");
		if ($dbComgateDataStatus != false && $dbComgateDataStatus->getSerial() == 1) {
			$status = $dbComgateDataStatus->getVerifiedToken();
		}
		
		$this->view->assign("city", $city);
		$this->view->assign("legalform", $legalform);
		$this->view->assign("status", $status);
		
	
		// Call webservice
		$matches = null;
		if ($searchDefault != null && $searchDefault != "") {
			$matches = $this->doWebserviceSearch($searchDefault, $countryDefault);
		}
		$this->view->assign("matchList", $matches);
		
		// Check whether manual open of win is allowed 
		
		$winOpenAllowed = false;
		if ($legalform != "" && $legalform != null && is_string($countryDefault) && strlen($countryDefault) == 2) {
			$winOpenAllowed = $this->checkAllowedToOpenWin($countryDefault, $legalform);
		}
		
		$this->view->assign("winOpenAllowed", $winOpenAllowed);
	}
	
	/**
	 * 
	 * @param string $searchName
	 * @param string $countryCode
	 */
	public function cimsysSearchNoWinAction ($searchName, $countryCode) {
		
		$matches = null;
		if ($searchName != null && $searchName != "") {
			$matches = $this->doWebserviceSearch($searchName, $countryCode);
		}
		$this->view->assign("matchList", $matches);
	}
	
	/**
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 */
	public function openNewCompanyNoWinAction (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate) {
		
		
		$status = "";
		$country = "";
		$name = "";
		$legalform = "";
		
		$dbComgateDataName = $this->getDbComgateDataName($dbComgate);
		if ($dbComgateDataName != false && $dbComgateDataName->getSerial() == 1) {
			$name = $dbComgateDataName->getVerifiedToken();
		}

		$dbComgateDataCountry = $this->getDbComgateDataIdentOrStandard("Address", "Country_code", $dbComgate);
		if ($dbComgateDataCountry != false && $dbComgateDataCountry->getSerial() == 1) {
			$country = $dbComgateDataCountry->getVerifiedToken();
		}
		
		$dbComgateDataLegalform = $dbComgate->getDbComgateDataWithSectionAndSpecifier("Predicate", "Legalform_code");
		if ($dbComgateDataLegalform != false && $dbComgateDataLegalform->getSerial() == 1) {
			$legalform = $dbComgateDataLegalform->getVerifiedToken();
		}
		
		$dbComgateDataStatus = $dbComgate->getDbComgateDataWithSectionAndSpecifier("Predicate", "Companystatus_code");
		if ($dbComgateDataStatus != false && $dbComgateDataStatus->getSerial() == 1) {
			$status = $dbComgateDataStatus->getVerifiedToken();
		}
		
		
		if ($legalform == "") {
			$legalform = "XXX";
		}
		
		
		if (strlen($name) < 1 || strlen($country) != 2) {
			return "fail";
		}
		
		ComSysLogger::getLogger()->log("Open new company " . $name . " " . $country . " " . $legalform . " " . $status);
		$arguments = array($country, $name, $legalform, "", $status,"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",0);
		
		$win = $this->doSoapRequest("insertIntoIdent", $arguments);
		ComSysLogger::getLogger()->log("Created new WIN " . $win);
			
		// Check WIN is ok.
		ComSysLogger::getLogger()->log("Created new WIN " . strlen($win));
		if (is_string($win) && strlen($win) == 12) {
			$dbComgate->setVerifiedStatus(9);
			$dbComgate->setWin(9);
		} else {
			return "fail";
		}

		$this->dbComgateRepository->update($dbComgate);
		$this->persistenceManager->persistAll();
		$this->destroySoapSession();
		return $win;
	}
	
	
	/**
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 */
	public function inactivateCompanyNoWinAction (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate) {
		
		// Delete dbComgateData elements of the dbComgateComp object
		$dbComgateDataElements = $dbComgate->getDbComgateData();
		foreach ($dbComgateDataElements as  $dbComgateData) {
			$this->dbComgateDataRepository->remove($dbComgateData);
		}

		$dbComgate->setVerifiedStatus(11);
		$dbComgate->setInactive(1);
		$this->dbComgateRepository->update($dbComgate);
		$this->persistenceManager->persistAll();
		return "ok";
	}
	
	/**
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 * @param string $win
	 * @return string
	 */
	public function setWinForDbComgateNoWinAction (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate, $win) {
		
		$dbComgate->setWin($win);
		$dbComgate->setVerifiedStatus(9);
	
		$this->dbComgateRepository->update($dbComgate);
		$this->persistenceManager->persistAll();
		$this->destroySoapSession();
		return "ok";
	}
	
	/**
	 * Returns either the data element with Section/Specifier or Ident/SectionSpecifier
	 * 
	 * @param string $section
	 * @param string $specifier
	 * @return \TYPO3\FLOW3\Persistence\Doctrine\Proxies\__CG__\Worldbox\ComSys\Domain\Model\DbComgateData
	 */
	private function getDbComgateDataIdentOrStandard ($section, $specifier, $dbComgate) {
		
		//  Ident/AddressCountry_code or Address/Country_code
		$dbComgateData= $dbComgate->getDbComgateDataWithSectionAndSpecifier($section, $specifier);
		if ($dbComgateData != false && $dbComgateData->getSerial() == 1) {
			return $dbComgateData;
		} else {
				
			$dbComgateData = $dbComgate->getDbComgateDataWithSectionAndSpecifier("Ident", $section . $specifier);
			if ($dbComgateData != false && $dbComgateData->getSerial() == 1) {
				return $dbComgateData;
			}
		}
		return false;
	}
	
	private function getDbComgateDataName ($dbComgate) {
		$dbComgateDataName = $dbComgate->getDbComgateDataWithSectionAndSpecifier("Name", "Name");
		if ($dbComgateDataName != false && $dbComgateDataName->getSerial() == 1) {
			return $dbComgateDataName;
		} else {
				
			$dbComgateDataName = $dbComgate->getDbComgateDataWithSectionAndSpecifier("Ident", "Name");
			if ($dbComgateDataName != false && $dbComgateDataName->getSerial() == 1) {
				return $dbComgateDataName;
			}
		}
		return false;
	}
	
}
