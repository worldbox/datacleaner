<?php
namespace Worldbox\ComSys\Controller;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */



use Worldbox\ComSys\Log\ComSysLogger;

use TYPO3\FLOW3\Persistence\Doctrine\PersistenceManager;

use Worldbox\ComSys\Domain\Model\DbComgate;

use Worldbox\ComSys\Domain\Model\DbComgateData;

use Worldbox\ComSys\Domain\Model\LinkCityCountry;

use TYPO3\FLOW3\Annotations as FLOW3;

use Doctrine\Common\DateTime\DateTime;

use TYPO3\FLOW3\Log\Logger;



/**
 * Checklists controller for the Worldbox.ComSys package 
 *
 * @FLOW3\Scope("singleton")
 */
class ChecklistsController extends \TYPO3\FLOW3\MVC\Controller\ActionController {

	
	/**
	 * @FLOW3\Inject
	 * @var Worldbox\ComSys\Log\ComSysLogger
	 */
	protected $comSysLogger;
	
	/**
	 * @FLOW3\Inject
	 * @var Worldbox\ComSys\Domain\Repository\DbProgRepository
	 */
	protected $dbProgRepository;
	
	/**
	 * @FLOW3\Inject
	 * @var Worldbox\ComSys\Domain\Repository\DbComgateRepository
	 */
	protected $dbComgateRepository;
	
	
	/**
	 * @FLOW3\Inject
	 * @var Worldbox\ComSys\Domain\Repository\DbComgateDataRepository
	 */
	protected $dbComgateDataRepository;
	
	/**
	 * @FLOW3\Inject
	 * @var Worldbox\ComSys\Domain\Repository\LinkCityCountryRepository
	 */
	protected $linkCityCountryRepository;
	
	
	
	/**
	 * @FLOW3\Inject
	 * @var \TYPO3\FLOW3\Log\Logger
	 */
	protected $logger;
	

	/**
	 * Index action
	 *
	 * @return void
	 */
	public function indexAction() {
		
		
		
		$dbProgs = $this->dbProgRepository->findAll();
		$this->view->assign("dbProgs", $dbProgs);
	}
	
	
	/**
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 */
	public function loadDbComgateAction (\Worldbox\ComSys\Domain\Model\DbProg $dbProg) {
			
		$dbComgateList = $this->dbComgateRepository->getCurrentDbComgate($dbProg->getProgCode());

		if ($dbComgateList->count() > 0) {
			$this->view->assign("dbComgate", $dbComgateList->getFirst());
		}
		
		$this->view->assign("title", $dbProg->getProgname());		
		$this->view->assign("numberOfEntries", $dbProg->getNumberOfDbComgateErrors());
		$this->view->assign("dbProg", $dbProg);
		
	}
	
	
	/**
	 * Returns the number of errors in the given DbProg
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 */               	
	public function numberOfErrorsInProgAction (\Worldbox\ComSys\Domain\Model\DbProg $dbProg) {
		$this->view->assign ("numberOfErrors", $dbProg->getNumberOfDbComgateErrors());
	}
	
	
	/**
	 * Returns an dbComgate object parsed in a template.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 */
	public function editAction (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate) {
	
		$dbComgateData 	= $dbComgate->getDbComgateData();
		$dbProg 		= $dbComgate->getDbProg(); 
		
		$this->view->assign ("dbComgate", $dbComgate);
		$this->view->assign ("dbProg", $dbProg);
		$this->view->assign ("countries", $this->linkCityCountryRepository->getCountryList());
		$this->view->assign ("availableSections", $dbComgate->getSections());
		$this->view->assign ("dbComgateDataList", $dbComgate->getDbComgateDataGroupedBySection());
		
	}
	
	
	/**
	 * Saves the given $dbComgate object. The method checks, whether
	 * there is still an error. If yes a message is returned and
	 * the object is not saved. If no dbComgateData element of $dbComgate
	 * data has an error, the verifiedStatus is set to 0.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 */
	public function saveDbComgateAction (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate) {
		
		if ($dbComgate->hasError()) {
			return "0";
		} else {
			$dbComgate->setVerifiedStatus(0);
		
			$this->dbComgateRepository->update($dbComgate);	
			$this->persistenceManager->persistAll();
		}
		return "1";
	}
	
	
	/**
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateData $dbComgateData
	 * @param string $newOriginal
	 */
	public function saveDbComgateDataAction (DbComgateData $dbComgateData, $newOriginal) {
		
		
		$dbComgateData->setVerifiedStatus(0);
		$dbComgateData->setOriginal($newOriginal);
		$this->dbComgateDataRepository->update($dbComgateData);
		return "";
	}
	
	/**
	 * Loads the next db comgate object and returns its listitem
	 * representation. If there is no next dbComgate an empty string
	 * is returned.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 * 
	 */
	public function loadNextDbComgateAction (\Worldbox\ComSys\Domain\Model\DbProg $dbProg, \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate) {
		
		$nextDbComgate = $this->dbComgateRepository->getNext($dbComgate, $dbProg->getProgCode());
		//return $dbProg->getProgCode();
		if ($nextDbComgate == false) {
			$this->view->assign("dbComgate", $dbComgate);
		} else {
			$this->view->assign("dbComgate", $nextDbComgate);
		}
		
		$this->view->assign("dbProg", $dbProg);
	}
	
	/**
	 * Loads the previous db comgate object and returns its listitem
	 * representation. If there is no next dbComgate an empty string
	 * is returned.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 * 
	 */
	public function loadPreviousDbComgateAction (\Worldbox\ComSys\Domain\Model\DbProg $dbProg, \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate) {
		
		$previousDbComgate = $this->dbComgateRepository->getPrevious($dbComgate, $dbProg->getProgCode());
		// return same db comgate if no next available
		if ($previousDbComgate == false) {
			$this->view->assign("dbComgate", $dbComgate);
		} else {
			$this->view->assign("dbComgate", $previousDbComgate);
		}
		
		$this->view->assign("dbProg", $dbProg);
	}
	
	
	/**
	 * Loads the first db comgate of a prog
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 * 
	 */
	public function loadFirstDbComgateAction (\Worldbox\ComSys\Domain\Model\DbProg $dbProg) {
		
		$dbComgateList = $this->dbComgateRepository->getCurrentDbComgate($dbProg->getProgCode());

		if ($dbComgateList->count() > 0) {
			$this->view->assign("dbComgate", $dbComgateList->getFirst());
		}
		$this->view->assign("dbProg", $dbProg);
	}
	
	
	/**
	 * Creates a new DbComgateData object for the given DbComgate object with the given
	 * $section.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgate $dbComgate
	 * @param string $section
	 * @param string $serial
	 * @param string $original
	 */
	public function addDbComgateDataAction (\Worldbox\ComSys\Domain\Model\DbComgate $dbComgate, $section, $serial, $original) {
		
		if ($dbComgate->sectionExists($section)) {
			// TODO: Error handling
		}

		// The default FK for dbComgateComp is 0 not null -> which leads to problems with doctrine insert. Thus:
		// Do manual insert because null is not allowed as default 
		$id = $this->dbComgateDataRepository->manualInsert();
		ComSysLogger::getLogger()->log("New fucking id " . $id);
		$newDbComgateData = $this->dbComgateDataRepository->findByIdentifier($id);
		
		$newDbComgateData->setDataelement($section);
		$newDbComgateData->setSerial($serial);
		$newDbComgateData->setOriginal($original);
		$newDbComgateData->setVerifiedStatus(0);
		$newDbComgateData->setEditDate(date("y-m-d"));
		$newDbComgateData->setUserCode("SYS");
	
		$dbComgate->addDbComgateData($newDbComgateData);
		$this->dbComgateDataRepository->update($newDbComgateData);
		$this->persistenceManager->persistAll();
		
		$this->view->assign("dbComgateData", $newDbComgateData);
	}
	
	
	/**
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateData $dbComgateData
	 */
	public function deleteDbComgateDataAction (DbComgateData $dbComgateData) {
		$this->dbComgateDataRepository->remove($dbComgateData);
	}
	
	/**
	 * @param int $dbComgateId
	 * @param string $zip
	 * @param string $original
	 * @param string $city
	 * @param string $country
	 * @param string $countryCode
	 * @param string $sourceUnique
	 * @param int $countryCodeId
	 */
	public function createNewLocationAction ($dbComgateId, $zip, $original, $city, $county, $countryCode, $sourceUnique, $countryCodeId) {
		
		// Link City Country definition from Form
		$linkCityCountry 	= $this->linkCityCountryRepository->getLinkCityCountry($original, $countryCode);
		
		if ($linkCityCountry == null) {
			
			$linkCityCountry = new LinkCityCountry();
			$linkCityCountry->setCountryCode($countryCode);
			$linkCityCountry->setSourceCity($original);
			$linkCityCountry->setCity($city);
			$linkCityCountry->setZip($zip);
			$linkCityCountry->setCounty($county);
			$linkCityCountry->setSourceUnique($sourceUnique);
			$linkCityCountry->setSourceDate(date("y-m-d"));
			$linkCityCountry->setSource("Checklist");
			$linkCityCountry->setSourceCityUc(strtoupper($city));
			
			
			$this->linkCityCountryRepository->add($linkCityCountry);
			$this->persistenceManager->persistAll();
		} else {
			$linkCityCountry->setZip($zip);
			$linkCityCountry->setCounty($county);
			$linkCityCountry->setSourceCity($original);
			$linkCityCountry->setSourceUnique($sourceUnique);
			$linkCityCountry->setSourceDate(date("y-m-d"));
			$linkCityCountry->setSource("Checklist");
			$linkCityCountry->setCity($city);
			
			$this->linkCityCountryRepository->update($linkCityCountry);
			$this->persistenceManager->persistAll();
		}
		
		$cityDbComgate = $this->dbComgateDataRepository->findByIdentifier($dbComgateId);
		$result = $this->linkCityCountryRepository->getLinkCityCountryList($city, $countryCode);
		
		// Sort the list that new LCC is on top
		$sortedResult = array();
		$sortedResult[] = $linkCityCountry;
		foreach ($result as $linkCityCountryResult) {
			if ($linkCityCountryResult->getTheid() !=  $linkCityCountry->getTheid()) {
				$sortedResult[] = $linkCityCountryResult;
			}
		}
		
		$this->view->assign("countries", $this->linkCityCountryRepository->getCountryList());
		$this->view->assign("linkCityCountries", $sortedResult);
		$this->view->assign("dbComgateData", $cityDbComgate);
		$this->view->assign("selectedLinkCityCountry", $linkCityCountry);

		
	}
	
	
/**
	 * Returns a list of LinkCityCountry records that match the given criteria.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateData $cityDbComgateData
	 * @param \Worldbox\ComSys\Domain\Model\LinkCityCountry $linkCityCountry
	 */
	public function selectLinkCityCountryAction ($cityDbComgateData, $linkCityCountry) {
		
		return json_encode($this->addLinkCityCountryToDbComgateData($cityDbComgateData, $linkCityCountry));
		
	
	}
	
	
	private function addLinkCityCountryToDbComgateData ($cityDbComgateData, $linkCityCountry) {
		
		// Holds the id of all DbComgateData elements that have to be reloaded due to change.
		$elementsToReload 		= array();
		
		// The section of the dbComgateData
		$section 				= $cityDbComgateData->getSectionOfDbComgateData();
		$sectionName			= $cityDbComgateData->getSection();
		$dbComgate				= $cityDbComgateData->getDbComgate();
		
		// All related DbComgateData items of the section (AddressCountry_code, City, Zip, County)
		$countryCodeDbComgate	= $section->getCountryCodeDbComgateData();
		$zipDbComgate			= $section->getZipDbComgateData();				
		$countyDbComgate		= $section->getCountyDbComgateData();
		
		
		// Update cityDbComgateData (Set Verified Token and Verified Status)
		$cityDbComgateData->setVerifiedToken($linkCityCountry->getCity());
		$cityDbComgateData->setVerifiedStatus(9);
		$cityDbComgateData->setOriginal($linkCityCountry->getCity());
		
		
		$this->dbComgateDataRepository->update($cityDbComgateData);
		
		// Update the conutry code data element (if exists) -> I think it should exista anyway.
		if ($countryCodeDbComgate != null) {
			$countryCodeDbComgate->setVerifiedToken($linkCityCountry->getCountryCode());
			$countryCodeDbComgate->setVerifiedStatus(9);
			$countryCodeDbComgate->setOriginal($linkCityCountry->getCountryCode());
			$elementsToReload[] = $countryCodeDbComgate->getTheid();
			$this->dbComgateDataRepository->update($countryCodeDbComgate);
		} else {
			$countryCodeDbComgate = new DbComgateData();
			$countryCodeDbComgate->setSerial($cityDbComgateData->getSerial());
			$countryCodeDbComgate->setDbComgate($dbComgate);
			$countryCodeDbComgate->setDataelement($sectionName . "/" . $section->getCountryCodeSpecifyer());
			$countryCodeDbComgate->setVerifiedStatus(9);
			$countryCodeDbComgate->setOriginal($linkCityCountry->getCountryCode());
			$countryCodeDbComgate->setVerifiedToken($linkCityCountry->getCountryCode());
			$countryCodeDbComgate->setUserCode("SYS");
			$countryCodeDbComgate->setEditdate(date("y-m-d"));
			$this->dbComgateDataRepository->add($countryCodeDbComgate);
			$this->persistenceManager->persistAll();
			$elementsToReload[] = $countryCodeDbComgate->getTheid();
		}
		
		
		// Create new dbComageData element representing zip if not yet exitst and zip is set in linkCityCountry
		if (strlen($linkCityCountry->getZip()) > 0) {
			
			if ($zipDbComgate == null) {
				
				$zipDbComage = new DbComgateData();
				$zipDbComage->setSerial($cityDbComgateData->getSerial());
				$zipDbComage->setDbComgate($dbComgate);
				$zipDbComage->setDataelement($sectionName . "/Zip");
				$zipDbComage->setVerifiedStatus(9);
				$zipDbComage->setOriginal($linkCityCountry->getZip());
				$zipDbComage->setVerifiedToken($linkCityCountry->getZip());
				$zipDbComage->setUserCode("SYS");
				$zipDbComage->setEditdate(date("y-m-d"));
				$this->dbComgateDataRepository->add($zipDbComage);
				$this->persistenceManager->persistAll();
				$elementsToReload[] = $zipDbComage->getTheid();
			} else {
				$zipDbComage->setOriginal($linkCityCountry->getZip());
				$this->dbComgateDataRepository->update($zipDbComage);
				$this->persistenceManager->persistAll();
			}
		}
		
		// Create new dbComageData element representing country if not yet exitst and country is set in linkCityCountry
		if (strlen($linkCityCountry->getCounty())) {
			
			if ($countyDbComgate == null) {
				$countyDbComgate = new DbComgateData();
				$countyDbComgate->setSerial($cityDbComgateData->getSerial());
				$countyDbComgate->setDbComgate($dbComgate);
				$countyDbComgate->setDataelement($sectionName . "/County");
				$countyDbComgate->setVerifiedStatus(9);
				$countyDbComgate->setOriginal($linkCityCountry->getCounty());
				$countyDbComgate->setVerifiedToken($linkCityCountry->getCounty());
				$countyDbComgate->setUserCode("SYS");
				$countyDbComgate->setEditdate(date("y-m-d"));
				
				$this->dbComgateDataRepository->add($countyDbComgate);
				$this->persistenceManager->persistAll();
				$elementsToReload[] = $countyDbComgate->getTheid();
			} else {
				$countyDbComgate->setOriginal($linkCityCountry->getCounty());
				$this->dbComgateDataRepository->update($countyDbComgate);
				$this->persistenceManager->persistAll();
			}
		}
		
		$elementsToReload[] = $cityDbComgateData->getTheid();
		return $elementsToReload;
	}
	
	
	/**
	 * Returns a List representation of the dbComgateData element with the given theid.
	 * @param int $dbComgateDataId
	 */
	public function reloadDbComgateDataElementAction ($dbComgateDataId) {
		
		$dbComgateData = $this->dbComgateDataRepository->findByIdentifier($dbComgateDataId);
		$this->view->assign ("countries", $this->linkCityCountryRepository->getCountryList());
		$this->view->assign ("dbComgateData", $dbComgateData);

	}
	
	
	/**
	 * Returns a list of LinkCityCountry records that match the given criteria.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateData $cityDbComgate
	 */
	public function getLinkCityCountryListAction ($cityDbComgate) {
			
		$section 					= $cityDbComgate->getSectionOfDbComgateData();
		
		$dbComageteDataCountryCode 	= $section->getCountryCodeDbComgateData();
		$city 						= $cityDbComgate->getOriginal();
		$countryCode 				= "";
			
		if ($dbComageteDataCountryCode != null && $dbComageteDataCountryCode->getVerifiedToken()) {
			$countryCode = $dbComageteDataCountryCode->getVerifiedToken();
		}

		$progCountryCode = $cityDbComgate->getDbComgate()->getDbProg()->getProgCountryCode();
		$result = $this->linkCityCountryRepository->getLinkCityCountryList($city, $countryCode);
		$this->view->assign("countries", $this->linkCityCountryRepository->getCountryList());
		$this->view->assign("linkCityCountries", $result);
		$this->view->assign("progCountryCode", $progCountryCode);
		$this->view->assign("dbComgateData", $cityDbComgate);
	}
	
	/**
	 * Returns a list of LinkCityCountry records that match the given criteria.
	 * 
	 * @param \Worldbox\ComSys\Domain\Model\DbComgateData $cityDbComgate
	 * @param string $cityName
	 * @param string $countryCode
	 */
	public function searchLinkCityCountryListAction ($cityDbComgate, $cityName, $countryCode) {
			
		
		$result = $this->linkCityCountryRepository->getLinkCityCountryList($cityName, false);
		
		$this->view->assign("progCountryCode", $countryCode);
		$this->view->assign("countries", $this->linkCityCountryRepository->getCountryList());
		$this->view->assign("linkCityCountries", $result);
		$this->view->assign("dbComgateData", $cityDbComgate);
	}
	
	
	
	
	
	public function initTestDataAction () {
		
		$dbComgate = $this->dbComgateDataRepository->findByIdentifier(23);
		$dbComgate->setVerifiedStatus(1);
		$this->dbComgateDataRepository->update($dbComgate);
		return "Done";
		
	}
	
	/**
	 * @param \Worldbox\ComSys\Domain\Model\DbProg $dbProg
	 * @param string $searchString
	 */
	public function searchDbComgateAction (\Worldbox\ComSys\Domain\Model\DbProg $dbProg, $searchString) {
		
		
		$dbComgateList = $this->dbComgateRepository->searchByUrl($dbProg->getProgCode(), $searchString);
		$dbComgateLast = $dbComgateList->toArray();
	
	
		if ($dbComgateList->count() > 0) {
			$this->view->assign("dbComgateList", $dbComgateList);
			$this->view->assign("dbComgateLast", $dbComgateLast[count($dbComgateLast)-1]);
		}
		
	}
	
}







?>