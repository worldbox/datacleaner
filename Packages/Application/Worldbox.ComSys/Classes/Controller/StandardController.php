<?php
namespace Worldbox\ComSys\Controller;

/*                                                                        *
 * This script belongs to the FLOW3 package "Worldbox.ComSys".            *
 *                                                                        *
 *                                                                        */

use TYPO3\FLOW3\Annotations as FLOW3;

/**
 * Standard controller for the Worldbox.ComSys package 
 *
 * @FLOW3\Scope("singleton")
 */
class StandardController extends \TYPO3\FLOW3\MVC\Controller\ActionController {

	/**
	 * @FLOW3\Inject
	 * @var Worldbox\ComSys\Domain\Repository\DbComgateRepository
	 */
	protected $dbComgateRepository;
	
	
	/**
	 * Index action
	 *
	 * @return void
	 */
	public function indexAction() {
		$this->redirect("index", "Checklists");
	}

}

?>