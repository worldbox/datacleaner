<?php
namespace Worldbox\ComSys\Controller;

use TYPO3\FLOW3\Annotations as FLOW3;

use Worldbox\ComSys\Log\ComSysLogger;

use TYPO3\FLOW3\Persistence\Doctrine\PersistenceManager;

use Worldbox\ComSys\Domain\Model\DbComgate;

use Worldbox\ComSys\Domain\Model\DbComgateData;

use Worldbox\ComSys\Domain\Model\LinkCityCountry;

use Doctrine\Common\DateTime\DateTime;

use TYPO3\FLOW3\Log\Logger;
/**
 * LoginController
 *
 * Handles all stuff that has to do with the login
 */
class LoginController extends \TYPO3\FLOW3\MVC\Controller\ActionController {
 
    /**
     * @var TYPO3\FLOW3\Security\Authentication\AuthenticationManagerInterface
     * @FLOW3\Inject
     */
    protected $authenticationManager;
 
    /**
     * @var TYPO3\FLOW3\Security\AccountRepository
     * @FLOW3\Inject
     */
    protected $accountRepository;
 
        /**
     * @var TYPO3\FLOW3\Security\AccountFactory
     * @FLOW3\Inject
     */
    protected $accountFactory;
 
    /**
     * index action, does only display the form
     */
    public function indexAction() {
        // do nothing, action only required to show form
    }
 
    /**
     * @throws TYPO3\FLOW3\Security\Exception\AuthenticationRequiredException
     * @return void
     */
    public function authenticateAction() {
    	
        try {
        
            $this->authenticationManager->authenticate();
            $this->flashMessageContainer->addMessage(new \TYPO3\Flow\Error\Error('Successfully logged in.'));
            $this->redirect('index', 'Login');
        } catch (\TYPO3\Flow\Security\Exception\AuthenticationRequiredException $exception) {
            $this->flashMessageContainer->addMessage(new \TYPO3\Flow\Error\Error('Wrong username or password.'));
            $this->flashMessageContainer->addMessage(new \TYPO3\Flow\Error\Error($exception->getMessage()));
            throw $exception;
        }
    }
 
    /**
     * @return void
     */
    public function registerAction() {
        // do nothing more than display the register form
    }
 
    /**
     * save the registration
     * @param string $name
     * @param string $pass
     * @param string $pass2
     */
    public function createAction($name, $pass, $pass2) {
 
        $defaultRole = 'Visitor';
 
        if($name == '' || strlen($name) < 3) {
            $this->flashMessageContainer->addMessage(new \TYPO3\Flow\Error\Error('Username too short or empty'));
            $this->redirect('register', 'Login');
        } else if($pass == '' || $pass != $pass2) {
            $this->flashMessageContainer->addMessage(new \TYPO3\Flow\Error\Error('Password too short or does not match'));
            $this->redirect('register', 'Login');
        } else {
 
            // create a account with password an add it to the accountRepository
            $account = $this->accountFactory->createAccountWithPassword($name, $pass, array($defaultRole));
            $this->accountRepository->add($account);
 
            // add a message and redirect to the login form
            $this->flashMessageContainer->addMessage(new \TYPO3\Flow\Error\Error('Account created. Please login.'));
            $this->redirect('index');
        }
 
        // redirect to the login form
        $this->redirect('index', 'Login');
    }
     
    public function logoutAction() {
        $this->authenticationManager->logout();
        $this->flashMessageContainer->addMessage(new \TYPO3\Flow\Error\Error('Successfully logged out.'));
        $this->redirect('index', 'Login');
    }
 
}