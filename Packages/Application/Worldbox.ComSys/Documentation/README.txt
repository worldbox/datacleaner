DataCleaner
===========



To Do:
---------

- Prog code bei country suche berücksichtigen.

- Anzahl von Fälle stimmt nicht

- Immer 2 Character country code verwenden: Sollte angeblich im verified token stehen (Auch sonst 
  bei DB_COMGATE_DATA nur aus verified token beziehn
  
- String bei WinMatch besser verarbeiten

- Check Win open allowed: Webservice call ist implementiert aber nicht eingeschaltet. Bin mir auch nicht
  sicher ob der funktioniert bzw. nicht sicher ob der parameter "legalform" einen speziellen Wert habe muss.

- Liste von Michel durchgehen

- In match List spate HR-Nummer


- Welcher Webservice muss ich verwenden?
	 * http://secure.newdev.worldbox.ch/ws_comgate/dataGateway.cfc?wsdl
	 * http://secure.stage.worldbox.ch/secure/wwwroot/ws_comgate/dataGateway.cfc?wsdl		Given by Anita I think
	 * http://secure.newProd.worldbox.ch/ws_comgate/dataGateway.cfc?wsdl 

Beachten:
--------

- Login/Logout
- Backup erstellen
- Console log entfernen

Login:
------

username = gan
password = anita22!


Locations:
----------

Local Dev:

http://localhost/ComSys/Web/index.php/worldbox.comsys/checklists/index

Dev Worldbox:

http://dev.comgate.worldboxglobal.ch/comsys/Web/main.php

http://www.worldboxglobal.ch/phpmyadmin/  


External Documentation:
-----------------------

https://drive.google.com/folderview?id=0B4oRI7eqhDheMlVWZTJ5YnRSVGs&usp=sharing


WINmatchComp:
=============


Search:
-------

There are two types of search:

1) Search for Parent Companies

The Search Parameter is Country (country_code). A search shows a List of all DB_COMGATE records
having a DB_COMGATE_COMP record with status 5. The DB_COMGATE_COMP record needs
to have a valid WIN (NOT NULL).

In the shown list the User can select a DB_COMGATE record and edit its cases (DB_COMGATE_COMP records).

Todo: Limit number of DB_COMGATE records shown (maybe paging).
-> Ask how many entries are expected.

 			WinMatchCompController				Repository
					|
					|
---------->	dbComgateCompListAction() ------------>			


			loadDbComgateCompForMatchAction ()


2) Directly search for Child Companies

Given a country (country_code) the method returs DB_COMGATE_COMP records having a DB_COMGATE_DATA
record with column DATAELEMENT = 'Ident/Coutry_code' and original=country_code.

----------> WinMatchCompController::searchDbComgateCompByCountryAction() 
			

Editing:
--------

A single DB_COMGATE_COMP record is presented to the user (Same for search method 1 and 2 - Only the list
from which this records are selected differs). The fileds shown of the record are:

Company Type:  	DB_COMGATE_COMP.ROLE_TYPE  # Whether ist a shareholder or a investment

Company Name:  	The DB_COMGATE_DATA element (column original) of the related DB_COMGATE record
				with DATAELEMENT = "Ident/Name"

Country:		The DB_COMGATE_DATA element (column original) of the related DB_COMGATE record
				with DATAELEMENT = "Ident/AddressCountry_code"
!!!				-> Problem there I have a three letter code (like "GBR") -> the country table I access has two letter codes ("GB").				
				-> !! Remove hack that just takes into account first two letters. 

At the time a DB_COMGATE_COMP record is loaded a search for potential matches (unsing WebService) is made.
The list of potential matches is shown to the User. 

To search for matches the values "Company Name" and "Country" as defined above are used. To optimeze
search results not the whole Company Name is used (Only the longest word in it).


The user can navigate (next/previous) through all DB_COMGATE_COMP records matching his search criterias.
The first record shown to the user is the one with the lowest THEID in the search result set.




SQL-Commands:
-------------
# Get all DB_COMGATE_DATA having DB_COMGATE with match_status = 5 (SLOW)
SELECT *
FROM `DB_COMGATE_DATA`
WHERE COMGATE_ID_COMP
IN (
	SELECT THEID
	FROM DB_COMGATE_COMP
	WHERE match_status =5
)

SELECT 
	theid 
FROM 
	DB_COMGATE_COMP 
LEFT JOIN 
	DB_COMGATE_DATA
ON 
	DB_COMGATE_COMP.theid = DB_COMGATE_DATA.comgate_id_comp 
	AND
	DB_COMGATE_COMP.match_status = 5


# Reset Test-Data
UPDATE DB_COMGATE_COMP SET match_status = 5 WHERE theid IN (588, 589, 590, 591, 592)


Webservice:
-----------

Makes use of some functions of a web service to access Company information.

Dokumentation:
https://docs.google.com/document/d/1baEOe7EuDrMkOGPT8ZbkP4pIzBvYDWd6eyGFF8BviGI/edit#

Using:

$url 		= "http://secure.newdev.worldbox.ch/ws_comgate/dataGateway.cfc?wsdl";
$client     = new SoapClient($url, array("trace" => 1, "exception" => 0)); 

// Open session
$sessionKey = $client->__soapCall("openSession", array("user" => "gan", "passowrd" => "anita22!"), NULL, null); 
// Do operations
$result = $client->__soapCall("getCompanyMatch", array("SessionKey" => $sessionKey, "name" => "Sulzer", "country" => "CH", "lang" => "de"), NULL, null); 
// Close session
$result = $client->__soapCall("closeSession", array("SessionKey" => $sessionKey), NULL, null); 




